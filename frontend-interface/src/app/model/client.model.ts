export interface ClientModel {
      activationDate: string;
      fingerprint: string ;
      firebaseToken: string;
      nickname: string;
}
