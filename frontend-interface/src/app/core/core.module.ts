import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientsPageComponent} from "./components/clients-page/clients-page.component";
import {ClientComponent} from "./components/clients-page/client/client.component";
import {MaterialModule} from "../shared/material/material.module";


@NgModule({
    declarations: [
        ClientsPageComponent,
        ClientComponent
    ],
    imports: [
        CommonModule,
        MaterialModule
    ]
})
export class CoreModule {
}
