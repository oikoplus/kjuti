import {Component, Input, OnInit} from '@angular/core';
import {ClientModel} from "../../../../model/client.model";

@Component({
    selector: 'app-client',
    styleUrls: ['client.component.scss'],
    template: `
        <mat-card class="client-card">
            <mat-card-header>
                <div mat-card-avatar  class="example-header-image">
                    <img mat-card-image src="/assets/img/avatar_placeholder.svg" alt="{{client.nickname}}">
                </div>
                <mat-card-title>{{client.nickname}}</mat-card-title>
                <mat-card-subtitle>Active since: {{client.activationDate|date}}</mat-card-subtitle>
            </mat-card-header>
            <div class="image-container">

            </div>
            <mat-card-content>
                <p>
                    Fingerprint: {{client.fingerprint}}
                </p>
                <p>
                    Token: {{client.firebaseToken}}
                </p>
            </mat-card-content>
            <mat-card-actions>
                <button mat-button>View details</button>
                <button mat-button>Send ping push</button>
            </mat-card-actions>
        </mat-card>
    `,
    styles: []
})
export class ClientComponent implements OnInit {

    @Input()
    client: ClientModel = {} as ClientModel;

    constructor() {
    }

    ngOnInit(): void {
    }

}
