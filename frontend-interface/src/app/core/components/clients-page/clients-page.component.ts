import {Component, OnInit} from '@angular/core';
import {ClientService} from "../../service/client.service";
import {ClientModel} from "../../../model/client.model";

@Component({
  selector: 'app-clients-page',
  template: `
      <div *ngFor="let client of clients">
        <app-client [client]=client></app-client>
      </div>
  `,
  styles: [
  ]
})
export class ClientsPageComponent implements OnInit {
  clients: ClientModel[] = [];

  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
    this.clientService.getClients().subscribe(clientsPayload => {
      this.clients = clientsPayload.payload;
    })
  }

}
