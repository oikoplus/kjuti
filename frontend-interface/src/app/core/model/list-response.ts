export interface ListResponse<T> {
  payload: T[];
}
