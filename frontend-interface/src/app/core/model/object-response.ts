export interface ObjectResponse<T> {
  payload: T;
}
