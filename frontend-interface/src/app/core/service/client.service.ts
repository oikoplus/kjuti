import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {ClientModel} from "../../model/client.model";
import {ListResponse} from "../model/list-response";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private _clientUrl: string = environment.clientUrl;

  constructor( private http: HttpClient) { }

  getClients(): Observable<ListResponse<ClientModel>> {
  return this.http.get<ListResponse<ClientModel>>(this._clientUrl);
}

}
