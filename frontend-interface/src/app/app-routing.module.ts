import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./core/components/home/home.component";
import {ClientsPageComponent} from "./core/components/clients-page/clients-page.component";


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'clients', component: ClientsPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
