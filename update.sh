#!/bin/bash
set -a
source .env
set +a

TAG=prod-latest

if [[ ${@} ]]
then
echo "*** *** ***"
echo "taking current ${APP_ID}-${@} down"
docker-compose stop ${@}
docker-compose rm ${@}
echo ""
echo ""
echo "*** *** ***"
echo "pulling new image(s)"
docker-compose pull ${@}
echo ""
echo ""
echo "*** *** ***"
echo "starting new ${APP_ID}-applications up"
docker-compose up -d
echo ""
echo "***"
else
echo "*** *** ***"
echo "taking current ${APP_ID} down"
docker-compose down
echo ""
echo ""
echo "*** *** ***"
echo "pulling new image(s)"
docker-compose pull
echo ""
echo ""
echo "*** *** ***"
echo "starting new ${APP_ID}-applications up"
docker-compose up -d
echo ""
echo "***"
fi


