#!/bin/sh
# wait-for-database.sh

set -e
cmd=""
for ARGUMENT in "$@"
do
  cmd="$cmd $ARGUMENT"
done

until liquibase validate; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 10
done

>&2 echo "Postgres is up - executing command"
echo "executing $cmd"
exec $cmd

