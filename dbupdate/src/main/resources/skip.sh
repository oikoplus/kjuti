#!/bin/sh
# wait-for-database.sh

set -e
cmd=""
for ARGUMENT in "$@"
do
  cmd="$cmd $ARGUMENT"
done

echo "Skipping database update... (command: $cmd)"
