import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/util/fluro_router.dart';
import 'package:kjuti_client/widgets/widgets.dart';

import 'blocs/auth/auth_bloc.dart';

class App extends StatefulWidget {
  App({Key key}) : super(key: key) {
    // Translations.missingKeyCallback = (key, locale) =>
    //     print("'$key': 'en_us: $key',");
    // Translations.missingTranslationCallback = (key, locale) => {};
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<App> {
  @override
  void initState() {
    super.initState();

    AppRouter appRouter = AppRouter(
      routes: AppRoutes.routes,
      notFoundHandler: AppRoutes.routeNotFoundHandler,
    );
    appRouter.setupRoutes();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Kju:Ti',
        theme: ThemeData(
          fontFamily: 'Arial',
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        debugShowCheckedModeBanner: false,
        onGenerateRoute: AppRouter.router.generator,
        home: Scaffold(
            appBar: KjutiAppBar().buildTopBar(context),
            body: BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                if (state is AuthInitial) {
                  BlocProvider.of<AuthBloc>(context).add(GetFingerprint());
                }
                if (state is AuthLoadingState) {
                  return Center(
                      child: Container(
                          padding: EdgeInsets.only(top: 100),
                          child: Column(children: [
                            CircularProgressIndicator(),
                            Container(
                                padding: EdgeInsets.only(top: 20),
                                child: Text(state.message ?? 'Loading...'))
                          ])));
                }
                if (state is AuthErrorState) {
                  return ErrorBox(error: state);
                }
                //
                if (state is FetchProfileState) {
                  BlocProvider.of<AuthBloc>(context)
                      .add(LoadProfile(fingerprint: state.fingerprint));
                  return Center(child: Text('loading profile'));
                }
                if (state is LoadingProfileSuccessState ||
                    state is RegisterSuccessState) {
                  return Home();
                }
                //
                if (state is NewProfileState) {
                  return RegisterWidget(fingerprint: state.fingerprint);
                }
                return Container();
              },
            ),
            bottomNavigationBar: KjutiAppBar().buildBottomBar(context)));
  }
}
