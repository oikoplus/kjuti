import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SnackbarService {
  static final SnackbarService _service = SnackbarService._internal();

  factory SnackbarService() {
    return _service;
  }

  SnackbarService._internal();

  void show(BuildContext context, String text,
      {Color textColor = Colors.black, SnackBarAction action}) async {
    final snackBar = SnackBar(
        backgroundColor: Colors.orange,
        content: Text(text, style: TextStyle(color: textColor, fontSize: 20)),
        action: action);

    await Future.delayed(Duration(milliseconds: 400));
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
