class Question {
  String _QHash;
  String _answerHash;
  bool _followUp;
  String _jsonData;
  String _question;
  String _title;
  String _answerType;

  String get QHash => _QHash;
  String get answerHash => _answerHash;
  bool get followUp => _followUp;
  String get jsonData => _jsonData;
  String get question => _question;
  String get title => _title;
  String get answerType => _answerType;

  Question({
      String QHash,
      String answerHash, 
      bool followUp, 
      String jsonData, 
      String question, 
      String title, 
      String answerType}){
    _QHash = QHash;
    _answerHash = answerHash;
    _followUp = followUp;
    _jsonData = jsonData;
    _question = question;
    _title = title;
    _answerType = answerType;
}

  Question.fromJson(dynamic json) {
    _QHash = json["QHash"];
    _answerHash = json["answerHash"];
    _followUp = json["followUp"];
    _jsonData = json["jsonData"];
    _question = json["question"];
    _title = json["title"];
    _answerType = json["answerType"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["QHash"] = _QHash;
    map["answerHash"] = _answerHash;
    map["followUp"] = _followUp;
    map["jsonData"] = _jsonData;
    map["question"] = _question;
    map["title"] = _title;
    map["answerType"] = _answerType;
    return map;
  }
}
