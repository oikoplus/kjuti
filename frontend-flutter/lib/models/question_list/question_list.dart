import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:kjuti_client/models/models.dart';

QuestionList questionListFromJson(String str) => QuestionList.fromJson(json.decode(str));

String questionListToJson(QuestionList data) => json.encode(data.toJson());

class QuestionList extends Equatable {
  final List<Question> questions;

  QuestionList({this.questions});

  factory QuestionList.fromJson(Map<String, dynamic> json) => QuestionList(
    questions: json["questions"],
  );

  Map<String, dynamic> toJson() => {
    "questions": List
  };

  @override
  List<Object> get props => [questions];
}
