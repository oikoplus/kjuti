// To parse this JSON data, do
//
//     final client = clientFromJson(jsonString);

import 'dart:convert';

Profile clientFromJson(String str) => Profile.fromJson(json.decode(str));

String clientToJson(Profile data) => json.encode(data.toJson());

class Profile {
  Profile({
    this.activationDate,
    this.fingerprint,
    this.firebaseToken,
    this.nickname,
  });

  DateTime activationDate;
  String fingerprint;
  String firebaseToken;
  String nickname;

  factory Profile.fromJson(Map<String, dynamic> json) =>
      Profile(
        activationDate: DateTime.parse(json["activationDate"]),
        fingerprint: json["fingerprint"].toString(),
        firebaseToken: json["firebaseToken"],
        nickname: json["nickname"],
      );

  Map<String, dynamic> toJson() =>
      {
        "activationDate": activationDate.toIso8601String(),
        "fingerprint": fingerprint.toString(),
        "firebaseToken": firebaseToken,
        "nickname": nickname,
      };
}
