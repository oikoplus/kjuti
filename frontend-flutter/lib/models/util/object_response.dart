import 'dart:convert';

ObjectResponse objectResponseFromJson(String str) => ObjectResponse.fromJson(json.decode(str));

String objectResponseToJson(ObjectResponse data) => json.encode(data.toJson());

class ObjectResponse<T> {
  ObjectResponse({
    this.payload
  });
  T payload;

  factory ObjectResponse.fromJson(Map<String, dynamic> json) =>
      ObjectResponse(
        payload: json["payload"]
      );

  Map<String, dynamic> toJson() =>
      {
        "payload": payload
      };
}
