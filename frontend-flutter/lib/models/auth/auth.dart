import 'dart:convert';

import 'package:equatable/equatable.dart';

Auth authFromJson(String str) => Auth.fromJson(json.decode(str));

String authToJson(Auth data) => json.encode(data.toJson());

class Auth extends Equatable {
  final DateTime date;


  Auth({
    this.date,

  });

  factory Auth.fromJson(Map<String, dynamic> json) => Auth(
        date: DateTime.parse(json["date"]),

      );

  Map<String, dynamic> toJson() => {
        "activationDate": date.toIso8601String(),

      };

  @override
  List<Object> get props =>
      [date];
}
