import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/repo/repositories.dart';

import 'app.dart';
import 'blocs/simple_bloc_observer.dart';


void main() async {
  Bloc.observer = SimpleBlocObserver();
  final AuthRepository authRepo = AuthRepository(
    authApiClient: AuthApiClient(
      httpClient: http.Client(),
    ),
  );
  final QuestionsRepository questionsRepo = QuestionsRepository(
    apiClient: QuestionsApiClient(
      httpClient: http.Client(),
    ),
  );
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(create: (context) => AuthBloc(authRepository: authRepo)),
        BlocProvider<QuestionsBloc>(create: (context) => QuestionsBloc(questionsRepository: questionsRepo))
      ],
        child: App()),
  );
}
