import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/repo/repositories.dart';

class QuestionsBloc extends Bloc<QuestionsEvent, QuestionsState> {
  final QuestionsRepository questionsRepository;

  QuestionsBloc({@required this.questionsRepository})
      : assert(questionsRepository != null),
        super(QuestionListInitial());

  QuestionList _questionList;
  QuestionList get questionList => _questionList;

  @override
  Stream<QuestionsState> mapEventToState(
    QuestionsEvent event,
  ) async* {
    try {
      if (event is LoadTrees) {
        yield TreesLoadingState();
        _questionList =
            await questionsRepository.getAllOpenTrees(event.fingerprint);
        if (_questionList != null && _questionList.questions.isEmpty) {
          yield TreeListEmpty();
        } else {
          yield OpenTreeListLoaded(questionList: questionList);
        }
      }
    } on Error catch (e, stack) {
      print('Error: ' + e.toString());
    } on Exception catch (e, stack) {
      print('Exception: ' + e.toString());
    }
  }

  // ignore: missing_return
  Future<Question> getCurrentByHash(String qHash) async {
    try {
      return await questionsRepository.getTreeByHash(qHash);
    } on Error catch (e, stack) {
      print('Error: ' + e.toString());
    } on Exception catch (e, stack) {
      print('Exception: ' + e.toString());
    }
  }
}
