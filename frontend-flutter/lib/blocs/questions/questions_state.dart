import 'package:equatable/equatable.dart';
import 'package:kjuti_client/models/models.dart';
import 'package:meta/meta.dart';

abstract class QuestionsState extends Equatable {
  const QuestionsState();

  final QuestionList questionList = null;

  @override
  List<Object> get props => [questionList];
}

class QuestionListInitial extends QuestionsState {}

class TreesLoadingState extends QuestionsState {}

class OpenTreeListLoaded extends QuestionsState {
  final QuestionList questionList;

  const OpenTreeListLoaded({@required this.questionList})
      : assert(questionList != null);

  @override
  List<Object> get props => [questionList];
}

class TreeListEmpty extends QuestionsState {}

class QuestionListLoadingError extends QuestionsState {}

class QuestionTreeLoaded extends QuestionsState {
  final Question question;

  const  QuestionTreeLoaded({@required this.question});

  @override
  List<Object> get props => [question];
}
