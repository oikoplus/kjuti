import 'package:equatable/equatable.dart';

abstract class QuestionsEvent extends Equatable {
  const QuestionsEvent();
}

class LoadTrees extends QuestionsEvent {
  final String fingerprint;

  const LoadTrees({this.fingerprint});
  @override
  List<Object> get props => [fingerprint];
}

class LoadQuestionTree extends QuestionsEvent {
  final String qHash;

  const LoadQuestionTree({this.qHash});
  @override
  List<Object> get props => [qHash];
}
