import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:kjuti_client/models/models.dart';

abstract class AuthState extends Equatable {
  const AuthState();
  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoadingState extends AuthState {
  final String message;

  const AuthLoadingState({this.message});

  @override
  List<Object> get props => [message];
}

class FetchProfileState extends AuthState {
  final String fingerprint;

  const FetchProfileState({this.fingerprint});

  @override
  List<Object> get props => [fingerprint];
}

class NewProfileState extends AuthState {
  final String fingerprint;

  const NewProfileState({this.fingerprint});

  @override
  List<Object> get props => [fingerprint];
}

class LoadingProfileSuccessState extends AuthState {
  final Profile profile;

  const LoadingProfileSuccessState({this.profile}) : assert(profile != null);

  @override
  List<Object> get props => [profile];
}


class PermissionGranted extends AuthState {
  final int fingerprint;
  final String token;
  final String nickname;

  const PermissionGranted(
      {@required this.fingerprint, this.token, this.nickname});

  @override
  List<Object> get props => [fingerprint, token, nickname];
}

class RegisterSuccessState extends AuthState {
  final Profile profile;

  const RegisterSuccessState({this.profile}) : assert(profile != null);

  @override
  List<Object> get props => [profile];
}

class AuthErrorState extends AuthState {
  final String message;
  final String stack;
  final Error error;

  const AuthErrorState({this.message, this.stack, this.error});

  @override
  List<Object> get props => [message, stack, error];
}
