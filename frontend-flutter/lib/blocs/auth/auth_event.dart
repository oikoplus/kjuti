import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:kjuti_client/models/models.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class GetFingerprint extends AuthEvent {
  @override
  List<Object> get props => [];
}

class LoadProfile extends AuthEvent {
  final String fingerprint;

  const LoadProfile({@required this.fingerprint}) : assert(fingerprint != null);

  @override
  List<Object> get props => [fingerprint];
}

class ReadyToRegister extends AuthEvent {
  final Profile profile;

  const ReadyToRegister({@required this.profile}) : assert(profile != null);

  @override
  List<Object> get props => [profile];
}

class RequestingPermission extends AuthEvent {
  const RequestingPermission();

  @override
  List<Object> get props => [];
}

class AuthErrorEvent extends AuthEvent {
  final String message;
  final String stack;
  final Error error;

  const AuthErrorEvent({this.message, this.stack, this.error});

  @override
  List<Object> get props => [message, stack, error];
}
