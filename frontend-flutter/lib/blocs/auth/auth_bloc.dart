import 'dart:async';
// ignore: avoid_web_libraries_in_flutter
import 'dart:js' as js;

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/repo/repositories.dart';
import 'package:kjuti_client/util/exception/exceptions.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;

  AuthBloc({@required this.authRepository})
      : assert(authRepository != null),
        super(AuthInitial());

  Profile _profile;
  Profile get profile => _profile;

  String _fingerprint;
  String get fingerprint => _fingerprint;

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    try {
      if (event is GetFingerprint) {
        yield AuthLoadingState(message: 'Getting fingerprint');
        try {
          _fingerprint =
              await js.context.callMethod('getFingerPrint').toString();
          yield FetchProfileState(fingerprint: _fingerprint.toString());
        } catch (e, stack) {
          print(e);
          yield AuthErrorState(
              message: 'Could not get fingerprint for device',
              stack: stack.toString());
        }
      }

      if (event is LoadProfile) {
        yield AuthLoadingState(message: 'Loading profile...');
        try {
          _profile = await authRepository.loadProfile(event.fingerprint);
          yield LoadingProfileSuccessState(profile: profile);
        } on ProfileNotFoundException {
          yield NewProfileState(fingerprint: event.fingerprint);
        } on GeneralException {
          yield AuthErrorState(message: 'Could not access profile api');
        }
      }

      if (event is ReadyToRegister) {
        yield AuthLoadingState(message: 'Registering...');
        try {
          _profile = await authRepository.register(event.profile);
          yield RegisterSuccessState(profile: profile);
        } on GeneralException {
          yield AuthErrorState(message: 'Could not register');
        }
      }

      if (event is AuthErrorEvent) {
        yield AuthErrorState(error: event.error);
      }
    } on Exception catch (e) {}
  }

  Future<int> _getFingerprint() async {
    return await js.context.callMethod('getFingerPrint');
  }

  void setFingerprint() {
    _getFingerprint().then((value) => _fingerprint = value.toString());
  }
}
