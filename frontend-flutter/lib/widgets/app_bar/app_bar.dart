import 'dart:js' as js;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjuti_client/blocs/auth/auth_bloc.dart';
import 'package:kjuti_client/models/models.dart';

class KjutiAppBar {
  AppBar buildTopBar(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      leadingWidth: 200,
      leading: Container(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
            SizedBox(
                height: 60, // Your Height
                child: Image.asset('splash_logo.jpg', fit: BoxFit.contain)),
            // SizedBox(
            //   width: 10,
            // ),
            // Text('Hi, Andi Loshi')
          ])),
      backgroundColor: Colors.white,
      actions: <Widget>[
        FlatButton(
            onPressed: () => null,
            child: Row(children: [
              Text(
                "About ",
              ),
              Icon(Icons.info, color: Colors.orange),
            ]))
      ],
    );
  }

  PreferredSize buildHomeMenuBar(BuildContext context) {
    Profile _profile = BlocProvider.of<AuthBloc>(context).profile;
    return PreferredSize(
      preferredSize: Size.fromHeight(35.0), // here the desired height
      child: AppBar(
        title: Text('Hi ${_profile?.nickname ?? ''}!',
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
        backgroundColor: Color.fromRGBO(66, 165, 245, 1),
        actions: <Widget>[
          FlatButton(
              onPressed: () => null,
              child: Row(children: [
                Text(
                  "Profile ",
                  style: TextStyle(color: Colors.white),
                ),
                Icon(Icons.account_circle, color: Colors.white),
              ]))
        ],
      ),
    );
  }

  AppBar buildQuestionMenuBar(BuildContext context, String title) {
    return AppBar(
      title: Text(title),
      backgroundColor: Color.fromRGBO(66, 165, 245, 1),
      actions: <Widget>[
        // FlatButton(
        //     onPressed: () => null,
        //     child: Row(children: [
        //       Text(
        //         "Profile ",
        //         style: TextStyle(color: Colors.white),
        //       ),
        //       Icon(Icons.account_circle, color: Colors.white),
        //     ]))
      ],
    );
  }

  BottomAppBar buildBottomBar(BuildContext context) {
    return BottomAppBar(
        color: Colors.white,
        child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 2),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FlatButton(
                      onPressed: () => null,
                      child: Row(children: [
                        Icon(
                          Icons.privacy_tip,
                          color: Colors.orange,
                          size: 17,
                        ),
                        Text(
                          " Privacy",
                          style: TextStyle(fontSize: 15),
                        )
                      ])),
                  FlatButton(
                      onPressed: () => js.context.callMethod(
                          'open', ['https://oikoplus.com', '_blank']),
                      child: Row(children: [
                        Icon(
                          Icons.copyright,
                          color: Colors.orange,
                          size: 17,
                        ),
                        Text(
                          " 2020-21, OIKOPLUS",
                          style: TextStyle(fontSize: 15),
                        )
                      ]))
                ])));
  }
}
