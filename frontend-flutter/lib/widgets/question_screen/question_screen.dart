import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjuti_client/blocs/questions/questions_bloc.dart';
import 'package:kjuti_client/models/question/question.dart';
import 'package:kjuti_client/widgets/app_bar/app_bar.dart';

class QuestionScreen extends StatefulWidget {
  final String qHash;
  const QuestionScreen({Key key, @required this.qHash}) : super(key: key);

  @override
  _QuestionScreenState createState() => _QuestionScreenState();
}

class _QuestionScreenState extends State<QuestionScreen> {
  bool _isLoaded = false;
  Question _question;
  @override
  void initState() {
    BlocProvider.of<QuestionsBloc>(context)
        .getCurrentByHash(widget.qHash)
        .then((value) => () {
              _question = value;
              _isLoaded = true;
            });
    super.initState();
  }

  Icon _getLeading() {
    switch(_question.answerType) {
      case 'CAM':
        return Icon(Icons.camera_alt);
        break;

    }
  }

  @override
  Widget build(BuildContext context) {
    return _isLoaded
        ? Scaffold(
            appBar: KjutiAppBar()
                .buildQuestionMenuBar(context, _question.title ?? 'Question'),
            body: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: _getLeading(),
                    title: const Text('Card title 1'),
                    subtitle: Text(
                      'Secondary Text',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.start,
                    children: [
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('ACTION 1'),
                      ),
                      FlatButton(
                        textColor: const Color(0xFF6200EE),
                        onPressed: () {
                          // Perform some action
                        },
                        child: const Text('ACTION 2'),
                      ),
                    ],
                  ),
                  // Image.asset('assets/header_trimmed.jpg')
                ],
              ),
            ),
          )
        : Center(child: CircularProgressIndicator());
  }
}
