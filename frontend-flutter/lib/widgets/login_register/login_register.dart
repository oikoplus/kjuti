
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjuti_client/blocs/auth/auth_bloc.dart';
import 'package:kjuti_client/blocs/auth/auth_event.dart';
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/util/messaging/firebase.dart';
import 'package:kjuti_client/util/style/form_card.dart';

class RegisterWidget extends StatefulWidget {
  final String fingerprint;

  RegisterWidget({Key key, @required this.fingerprint}) : super(key: key);

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  final _nickNameController = TextEditingController();
  final _msg = FBMessaging.instance;

  bool _loadingPermission = false;
  bool _hasPermission = false;
  String _token = '';

  @override
  void initState() {
    super.initState();
    _msg.init();
  }

  void _registerClient() {
    var newProfile = new Profile(
        activationDate: new DateTime.now(),
        fingerprint: widget.fingerprint.toString(),
        firebaseToken: _token.toString(),
        nickname: _nickNameController.text);
    BlocProvider.of<AuthBloc>(context).add(ReadyToRegister(profile: newProfile));
  }

  void _gotToken(String token) {
    print('gotToken $token');
    if (token.length == 0) {
      return;
    }
    setState(() {
      _loadingPermission = false;
      _token = token;
      _hasPermission = true;
    });
  }

  void _handleError(Error e) {
    setState(() {
      _loadingPermission = false;
    });
    BlocProvider.of<AuthBloc>(context).add(AuthErrorEvent(error: e));
  }

  void _requestPermission() {
    setState(() {
      _loadingPermission = true;
    });

    _msg
        .init()
        .then((_) => setState(() {
              _loadingPermission = true;
            }))
        .then((_) async => _msg.getToken())
        .then((token) => _gotToken(token))
        .catchError((e) => _handleError(e));
  }

  @override
  Widget build(BuildContext context) {
    return FormCard(
        childWidget: Form(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
              padding: EdgeInsets.fromLTRB(20, 25, 20, 5),
              child: SelectableText('Welcome to KjuːTi!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 28,
                      fontWeight: FontWeight.bold))),
          Container(
              padding: EdgeInsets.only(top: 10),
              child: _hasPermission
                  ? Icon(Icons.cloud_done, size: 50, color: Colors.orange)
                  : Icon(Icons.cloud, size: 50, color: Colors.black)),
          Visibility(
              visible: !_hasPermission,
              child: Column(
                children: [
                  Visibility(
                      visible: !_loadingPermission,
                      child: Container(
                          padding: EdgeInsets.fromLTRB(30, 20, 30, 5),
                          child: SelectableText(
                            'We need your permission to be able to send you questions from the cloud!',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22,
                                fontWeight: FontWeight.normal),
                          ))),
                  Visibility(
                      visible: _loadingPermission,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                              child: CircularProgressIndicator()),
                          Container(
                              padding: EdgeInsets.fromLTRB(0, 20, 0, 20),
                              child: SelectableText('Please click on Allow',
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))),
                          Container(
                              padding: EdgeInsets.fromLTRB(30, 0, 30, 20),
                              child: SelectableText(
                                  'Processing might take a couple of seconds...',
                                  style: TextStyle(fontSize: 20))),
                        ],
                      )),
                  Visibility(
                      visible: !_loadingPermission,
                      child: Container(
                          padding: EdgeInsets.fromLTRB(0, 20, 0, 30),
                          child: FlatButton(
                            color: Colors.blue,
                            textColor: Colors.white,
                            onPressed:
                                _loadingPermission ? null : _requestPermission,
                            child: Padding(
                                padding: EdgeInsets.fromLTRB(10, 5, 10, 6),
                                child: Text('Grant permission',
                                    style: TextStyle(fontSize: 20))),
                          ))),
                ],
              )),
          Visibility(
              visible: _hasPermission,
              child: Column(children: [
                Container(
                    padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
                    child: SelectableText(
                      'Thank you!\n\n We are almost ready!',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 15),
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(30, 5, 30, 20),
                    child: TextFormField(
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.black, fontSize: 16),
                      decoration: InputDecoration(
                          hintText: 'Please provide a nickname'),
                      // autofocus: true,
                      controller: _nickNameController,
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(5, 5, 5, 20),
                    child: FlatButton(
                      color: Colors.blue,
                      textColor: Colors.white,
                      onPressed: _hasPermission ? _registerClient : null,
                      child: Text('Ok let\'s go!'),
                    ))
              ])),
        ],
      ),
    ));
  }
}
