import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/widgets/widgets.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KjutiAppBar().buildHomeMenuBar(context),
      body: ListView(children: [
        BlocBuilder<QuestionsBloc, QuestionsState>(
          builder: (context, state) {
            // INIT / LOADING
            if (state is QuestionListInitial) {
              BlocProvider.of<QuestionsBloc>(context)
                  .add(new LoadTrees(fingerprint: BlocProvider.of<AuthBloc>(context).profile.fingerprint));
              return Center(child: CircularProgressIndicator());
            }
            if (state is TreesLoadingState) {
              return Center(child: CircularProgressIndicator());
            }
            if (state is OpenTreeListLoaded || state is TreeListEmpty) {
              return QuestionListWidget();
            }
            if (state is QuestionListLoadingError) {
              return Center(
                child: Text('Loading error'),
              );
            }
            return Home();
          },
        )
      ]),
    );
  }
}
