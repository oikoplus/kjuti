import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/repo/repositories.dart';


class Splash extends StatelessWidget {
  final QuestionsRepository questionsRepo;
  final int fingerprint;

  Splash({Key key, this.questionsRepo, @required this.fingerprint});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            // if (state is AuthInitial && fingerprint != null) {
            //   BlocProvider.of<AuthBloc>(context)
            //       .add(new LoadProfile(fingerprint: fingerprint));
            // }
            //
            // if (state is AuthInitial || state is LoadingProfile) {
            //   return LoadingInfo(text: 'Loading', subText: 'profile');
            // }
            //
            // if (state is Registering) {
            //   return LoadingInfo(
            //     text: 'Registering your device...',
            //     subText: 'This might take some time ...',
            //   );
            // }
            //
            // if (state is OpenRegisterDialog) {
            //   return RegisterWidget(fingerprint: fingerprint);
            // }

            // if (state is PermissionGranted) {
            //   Profile newProfile = new Profile(
            //       activationDate: new DateTime.now(),
            //       fingerprint: state.fingerprint,
            //       firebaseToken: state.token.toString(),
            //       nickname:
            //       state.nickname != null ? state.nickname : '');
            //
            //   BlocProvider.of<AuthBloc>(context).add((new ReadyToRegister(
            //       profile: newProfile)));
            //
            //   return FormCard(
            //       childWidget: Padding(
            //           padding: EdgeInsets.fromLTRB(15, 15, 15, 15),
            //           child: Column(children: [Text('Registering')])));
            // }
            //
            // if (state is LoadingProfileSuccessState || state is RegisterSuccess) {
            //   return BlocProvider(
            //       create: (context) => QuestionsBloc(
            //           questionsRepository: questionsRepo,
            //           profile: state.profile),
            //       child: Home());
            // }
            //
            // if (state is RegisterFailure) {
            //   return InternalServerError(
            //     message: 'Could not authenticate',
            //   );
            // }

            return Splash(fingerprint: -1);
          },
        ),
      ),
    );
  }
}
