// ignore: avoid_web_libraries_in_flutter
import 'dart:convert';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart' as intl;
import 'package:kjuti_client/blocs/blocs.dart';
import 'package:kjuti_client/models/question/question.dart';
import 'package:kjuti_client/util/fluro_router.dart';

class QuestionListWidget extends StatefulWidget {
  QuestionListWidget({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _QuestionListWidgetState();
}

class _QuestionListWidgetState extends State<QuestionListWidget> {
  List<Question> _questions = [];

  @override
  void initState() {
    super.initState();
    _questions =
        BlocProvider.of<QuestionsBloc>(context).questionList?.questions ?? [];
  }

  Directionality getDirectionalText(String string) {
    final TextDirection dir =
        intl.Bidi.detectRtlDirectionality(utf8.decode(string.runes.toList()))
            ? TextDirection.rtl
            : TextDirection.ltr;
    print(utf8.decode(string.runes.toList()));
    print(dir.toString());
    return Directionality(
        textDirection: dir,
        child: Wrap(textDirection: dir, children: [
          Text(utf8.decode(string.runes.toList()),
              textAlign: TextAlign.center,
              softWrap: true,
              style: TextStyle(
                  color: Colors.white,
                  height: 1.25,
                  fontSize: 20,
                  fontWeight: FontWeight.bold))
        ]));
  }

  Icon _getLeading(String answerType) {
    switch (answerType) {
      case 'CAM':
        return Icon(Icons.camera_alt, size: 40, color: Colors.orange);
        break;
      case 'GEO':
        return Icon(Icons.map, size: 40, color: Colors.orange);
        break;
      default:
        return Icon(Icons.question_answer, size: 40, color: Colors.orange);
    }
  }

  _openQuestionTree(Question question) {
    AppRouter.router.navigateTo(
        context, "/${question.answerType.toLowerCase()}/${question.QHash}",
        transition: TransitionType.material);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(shrinkWrap: true, children: [
      Container(
          padding: EdgeInsets.only(bottom: 20),
          child: Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                Container(
                    padding: EdgeInsets.all(8),
                    child: ListTile(
                      leading: Icon(Icons.arrow_drop_down_circle),
                      title: const Text('Welcome back!',
                          style: TextStyle(fontSize: 18)),
                      subtitle: Text(
                        'You have ${_questions.length} open question tree(s) you can answer right now!',
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6), fontSize: 15),
                      ),
                    ))
              ],
            ),
          )),
      ListView.separated(
          shrinkWrap: true,
          itemCount: _questions.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              borderOnForeground: true,
              child: Column(
                children: <Widget>[
                  Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors
                              .blue, //                   <--- border color
                          width: 2.5,
                        ),
                      ),
                      child: ListTile(
                        leading: _getLeading(_questions[index].answerType),
                        title: Text(_questions[index].question,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold)),
                        trailing: RaisedButton(
                          onPressed: () {
                            _openQuestionTree(_questions[index]);
                          },
                          child: Text('Start!',
                              style:
                                  TextStyle(fontSize: 30, color: Colors.white)),
                          color: Colors.blue,
                        ),
                      )),
                  // Container(
                  //     color: Colors.blue,
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //       children: <Widget>[
                  //         TextButton(
                  //           child: Text('ANSWER',
                  //               style: TextStyle(
                  //                   color: Colors.white, fontSize: 20)),
                  //           onPressed: () =>
                  //               _openQuestionTree(_questions[index].QHash),
                  //         )
                  //       ],
                  //     )),
                ],
              ),
            );
          },
          separatorBuilder: (BuildContext context, int index) =>
              const Divider())
    ]);
  }
}
