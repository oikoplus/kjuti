import 'package:flutter/material.dart';

class InternalServerError extends StatelessWidget {
  final String message;

  InternalServerError({
    this.message,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
        child: ListView(
          children: [
            Container(
              child: Text("500 - Internal server error"),
            ),
            Container(
              child: Text(message),
            )
          ],
        )
    );
  }
}
