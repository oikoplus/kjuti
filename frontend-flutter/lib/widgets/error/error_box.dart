import 'package:flutter/material.dart';
import 'package:kjuti_client/blocs/auth/auth_state.dart';

class ErrorBox extends StatefulWidget {
  final AuthErrorState error;

  ErrorBox({Key key, this.error})
      : super(key: key);

  @override
  _ErrorBoxState createState() => _ErrorBoxState();
}

class _ErrorBoxState extends State<ErrorBox> {
  int statusCode;
  String message;
  String stack;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
                padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                child: Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ListTile(
                        leading:
                            Icon(Icons.report, size: 50, color: Colors.red),
                        title: Text('Internal error!'),
                        subtitle: Text(
                          'Fehlercode: ${statusCode}',
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.fromLTRB(16, 0, 16, 10),
                      //   child: Text(
                      //     'Bei Meldung bitte eine kurze Erklärung sowie nach Möglichkeit einen Screenshot beilegen und sicherstellen, dass die unten stehenden Details zur E-Mail hinzugefügt wurden.'.i18n,
                      //     style:
                      //         TextStyle(color: Colors.black.withOpacity(0.6)),
                      //   ),
                      // ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                        child: Text(
                          message ?? widget.error.toString(),
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
                        child: Text(
                          stack.toString(),
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                      ),
                      ButtonBar(
                        alignment: MainAxisAlignment.end,
                        children: [
                          // FlatButton(
                          //   onPressed: () async {
                          //     final body = 'Error Code: ${widget.statusCode}\n${widget.message}\n${widget.stack}';
                          //     final mailUrl = env().errorMailUrl(body);
                          //     if (await canLaunch(mailUrl)) {
                          //       await launch(mailUrl);
                          //       await launch(env().deployPath(),
                          //           forceSafariVC: true,
                          //           forceWebView: true,
                          //           webOnlyWindowName: '_self');
                          //     } else {
                          //       throw 'Could not launch mail client'.i18n;
                          //     }
                          //   },
                          //   child: Padding(
                          //       padding: const EdgeInsets.fromLTRB(5, 2, 5, 0),
                          //       child: Text('Per Email melden'.i18n)),
                          // ),
                          // RaisedButton(
                          //   onPressed: () async {
                          //     if (await canLaunch(env().deployPath())) {
                          //       await launch(env().deployPath(),
                          //           forceSafariVC: true,
                          //           forceWebView: true,
                          //           webOnlyWindowName: '_self');
                          //     } else {
                          //       throw 'Could not open %s'.i18n.fill([env().deployPath()]);
                          //     }
                          //   },
                          //   child: Padding(
                          //       padding: const EdgeInsets.fromLTRB(5, 2, 5, 0),
                          //       child: Text('Zurück zur Applikation')),
                          // )
                        ],
                      ),
                    ],
                  ),
                ))
          ]),
      // decoration: new BoxDecoration(
      //   boxShadow: [
      //     new BoxShadow(
      //       color: Colors.black,
      //       blurRadius: 20.0,
      //     ),
      //   ],
      // ),
    );
  }
}
