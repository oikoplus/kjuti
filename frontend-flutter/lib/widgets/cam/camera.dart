// ignore: avoid_web_libraries_in_flutter
import 'dart:async';
import 'dart:html';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:kjuti_client/models/question/question.dart';
import 'package:kjuti_client/widgets/app_bar/app_bar.dart';

class CameraPage extends StatefulWidget {
  final String qHash;
  const CameraPage({Key key, this.qHash}) : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  Widget _placeholderWidget = Container(
      padding: EdgeInsets.only(top: 50),
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Expanded(child: Icon(Icons.image, size: 100, color: Colors.grey)),
      ]));

  bool _hasWebcam = false;
  // Webcam widget to insert into the tree
  Widget _imageWidget;
  // VideoElement
  VideoElement _webcamVideoElement;
  bool _tookImage = false;

  Uint8List _imageToUpload;
  bool _pickedImage = false;

  bool _isLoaded = false;
  Question _question;

  @override
  void initState() {
    super.initState();
    _isLoaded = true;
    // BlocProvider.of<QuestionsBloc>(context)
    //     .getCurrentByHash(widget.qHash)
    //     .then((value) => () {
    //           _question = value;
    //           _isLoaded = true;
    //         });
    // _setupCam();
  }

  void _setupCam() {
    // Create a video element which will be provided with stream source
    _webcamVideoElement = VideoElement();
    // Register webcam
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
        'webcamVideoElement', (int viewId) => _webcamVideoElement);

    // Access the webcam stream
    window.navigator.getUserMedia(video: {'facingMode': 'environment'}).then(
        (MediaStream stream) {
      _webcamVideoElement.srcObject = stream;
      // Create video widget
      _hasWebcam = true;
      _imageWidget = Container(
          child: HtmlElementView(
              key: UniqueKey(), viewType: 'webcamVideoElement'));
    }).catchError((e) {
      print('COULD NOT OPEN CAM');
      _imageWidget = _placeholderWidget;
      print(e);
    });
  }

  // void fetchQData(String hash) async {
  //   print('fetching qData for qHash');
  //   print(hash);
  //   var url = env().deployPath() + 'api/q/' + hash;
  //   var response = await http.post(url);
  //   print('Response status: ${response.statusCode}');
  //   print('Response body: ${response.body}');
  // }

  _startFilePicker() async {
    InputElement uploadInput = FileUploadInputElement();
    uploadInput.accept = '.png,.jpg';
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final files = uploadInput.files;
      if (files.length == 1) {
        final file = files[0];
        FileReader reader = FileReader();

        reader.onLoadEnd.listen((e) {
          setState(() {
            _imageToUpload = reader.result;
            _imageWidget = Container(child: Image.memory(_imageToUpload));
            _pickedImage = true;
          });
        });

        reader.onError.listen((fileEvent) {
          // setState(() {
          //   option1Text = "Some Error occured while reading the file";
          // });
        });

        reader.readAsArrayBuffer(file);
      }
    });
  }

  void _uploadImage() {
    setState(() {
      _imageWidget = Center(child: CircularProgressIndicator());
    });
    new Timer(
        const Duration(seconds: 1),
        () => setState(() {
              _imageWidget = Container(
                  padding: EdgeInsets.only(top: 50),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                            child: Icon(Icons.check_circle,
                                size: 100, color: Colors.orange)),
                      ]));
              _tookImage = _pickedImage = false;
            }));
    new Timer(
        const Duration(milliseconds: 1250),
        () => FluroRouter.appRouter.navigateTo(context, '/home',
            transitionDuration: Duration(milliseconds: 200),
            transition: TransitionType.fadeIn));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KjutiAppBar().buildQuestionMenuBar(context, ''),
      body: _isLoaded
          ? Container(
              padding: EdgeInsets.all(5),
              child: Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.camera_alt, color: Colors.orange),
                      // title: SelectableText(_question.title,
                      title: SelectableText('Hello Kju:Ti!',
                          style: TextStyle(fontSize: 25)),
                      // subtitle: SelectableText(
                      //   'Secondary Text',
                      //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      // ),
                    ),
                    Container(
                        padding:
                            const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 15),
                        // child: SelectableText(_question.question,
                        child: SelectableText(
                            'Please take a picture of something interesting you can see from your position right now!',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 25,
                            ))),
                    Visibility(
                        visible: _imageWidget != null,
                        child: Expanded(child: _imageWidget)),
                    Visibility(
                        visible: _imageWidget == null,
                        child: Expanded(child: _placeholderWidget)),
                    Container(
                      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 10),
                      child: SelectableText(
                        'You can upload an image (.jpg or .png) from your device, or take a picture directly if your device has a camera and supports this.',
                        style: TextStyle(color: Colors.black.withOpacity(0.6)),
                      ),
                    ),
                    Visibility(
                        visible: _pickedImage || _tookImage,
                        child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            child: ButtonBar(
                              mainAxisSize: MainAxisSize.min,
                              alignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    padding: EdgeInsets.only(right: 45),
                                    child: FloatingActionButton(
                                      heroTag: 'cancel',
                                      backgroundColor: Colors.red,
                                      onPressed: () => setState(() {
                                        _imageToUpload = null;
                                        _tookImage = false;
                                        _pickedImage = false;
                                        _imageWidget = Container(
                                            child: _placeholderWidget);
                                      }),
                                      // ),
                                      tooltip: 'Delete image',
                                      child:
                                          Icon(Icons.highlight_off, size: 35),
                                    )),
                                Container(
                                    padding: EdgeInsets.only(left: 45),
                                    child: FloatingActionButton(
                                      heroTag: 'upload',
                                      backgroundColor: Colors.green,
                                      onPressed: _uploadImage,
                                      tooltip: 'Upload image',
                                      child: Icon(Icons.check_circle, size: 35),
                                    ))
                              ],
                            ))),
                    Visibility(
                        visible: !_pickedImage && !_tookImage,
                        child: Container(
                            padding: EdgeInsets.only(bottom: 15),
                            child: ButtonBar(
                              mainAxisSize: MainAxisSize.min,
                              alignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    child: FloatingActionButton(
                                  heroTag: 'upload',
                                  onPressed: _startFilePicker,
                                  // ),
                                  tooltip: 'Image upload',
                                  child: Icon(Icons.camera_alt, size: 35),
                                )),
                              ],
                            ))),
                  ],
                ),
              ))
          : Center(child: CircularProgressIndicator()),
    );
  }
}
