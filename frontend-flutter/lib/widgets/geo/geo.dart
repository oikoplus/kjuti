// ignore: avoid_web_libraries_in_flutter
import 'dart:async';

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kjuti_client/widgets/app_bar/app_bar.dart';

class GeoPage extends StatefulWidget {
  final String qHash;
  const GeoPage({Key key, this.qHash}) : super(key: key);

  @override
  _GeoPageState createState() => _GeoPageState();
}

class _GeoPageState extends State<GeoPage> {
  Set<Marker> _markers = {};
  bool _markerPlaced = false;
  bool _upload = false;

  @override
  void initState() {
    super.initState();
  }

  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _initPos = CameraPosition(
    target: LatLng(50.839316718475125, 4.339390143949924),
    zoom: 15,
  );

  void _createMarker(LatLng latLng) {
    setState(() {
      _markers = {};
      _markers.add(Marker(
        markerId: MarkerId("Here!"),
        icon: BitmapDescriptor.defaultMarker,
        position: LatLng(latLng.latitude, latLng.longitude),
      ));
      _markerPlaced = true;
    });
  }

  void _clearMarker() {
    setState(() {
      _markerPlaced = false;
      _markers = {};
    });
  }

  void _confirm() {
    setState(() {
      _upload = true;
      _markerPlaced = false;
    });
    Timer(
        const Duration(milliseconds: 500),
        () => FluroRouter.appRouter.navigateTo(context, '/home',
            transitionDuration: Duration(milliseconds: 200),
            transition: TransitionType.fadeIn));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: KjutiAppBar().buildQuestionMenuBar(context, ''),
        body: Container(
            padding: EdgeInsets.all(5),
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.map, color: Colors.orange),
                    title: SelectableText('Hello Kju:Ti!',
                        style: TextStyle(fontSize: 25)),
                  ),
                  Container(
                      padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 15),
                      // child: SelectableText(_question.question,
                      child: SelectableText(
                          'Where do you feel especially relaxed?',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 25,
                          ))),
                  Expanded(
                      child: GoogleMap(
                          mapType: MapType.hybrid,
                          initialCameraPosition: _initPos,
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                          markers: _markers,
                          onTap: (latLng) => _createMarker(latLng))),
                  Container(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 10),
                    child: SelectableText(
                      'Please place a marker to where the spot you wan\'t to mark!',
                      style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Visibility(
                      visible: _upload == true,
                      child: Container(
                          padding: EdgeInsets.only(top: 50),
                          child: Icon(Icons.check_circle,
                              size: 100, color: Colors.orange))),
                  Visibility(
                      visible: _markerPlaced == true,
                      child: Container(
                          padding: EdgeInsets.only(bottom: 15),
                          child: ButtonBar(
                            mainAxisSize: MainAxisSize.min,
                            alignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  padding: EdgeInsets.only(right: 45),
                                  child: FloatingActionButton(
                                    heroTag: 'cancel',
                                    backgroundColor: Colors.red,
                                    onPressed: _clearMarker,
                                    tooltip: 'Delete',
                                    child: Icon(Icons.highlight_off, size: 35),
                                  )),
                              Container(
                                  padding: EdgeInsets.only(left: 45),
                                  child: FloatingActionButton(
                                    heroTag: 'upload',
                                    backgroundColor: Colors.green,
                                    onPressed: _confirm,
                                    tooltip: 'Upload',
                                    child: Icon(Icons.check_circle, size: 35),
                                  ))
                            ],
                          ))),
                ],
              ),
            )));
  }
}
