import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AboutScreen extends StatelessWidget {
  Widget getQuestionText(String string) {
    return new Text(string,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ));
  }

  Widget getAnswerText(String string) {
    return new Text(string, overflow: TextOverflow.ellipsis, style: TextStyle(fontWeight: FontWeight.normal));
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(16.0),
      // width: c_width,
      child: new Column(
        children: <Widget>[
          getQuestionText("1. What is Kju:Ti?"),
          getAnswerText(
              "Kju:Ti is a digital communication tool that can be used in different use cases like urban planning or participation projects and can be enriched with different methods of empirical social research. Kju:Ti basically works as a questions and answering tool. It asks questions to the users and gets answers in return."),
          getQuestionText("2. How does Kju:Ti work?"),
          getAnswerText(
              "Kju:Ti asks Questions via Push-Notifications, which reduces the number of clicks needed for answering a question. Users don´t have to open the App, as the question and answering options are shown directly on the screen."),
          getQuestionText("3. What is the idea behind Kju:Ti?"),
          getAnswerText(
              "Conventional urban planning processes exclude many parts of the population because of (among other things) the high effort required, because of restrictions due to the language spoken, because of a location- and time-dependency or because of a lack of information. These hurdles are also called Participatory Gap. Kju:Ti wants to target these issues and offer an alternative. In Kju:Ti, the Participatory Gap is aimed at by a reduction of complexity, a simplified operation, a relative detachment from time and location and by allowing an easy implementation of different languages."),
          getQuestionText("4. How do users get in contact with Kju:Ti?"),
          getAnswerText(
              "Users can install Kju:Ti on their mobile device as well as using in on their computer desktop. The installing process is none per se. Users just have to click on a button for allowing notifications and they are good to go."),
          getQuestionText("5. How do users interact with Kju:Ti?"),
          getAnswerText(
              "Users interact with Kju:Ti through receiving and answering questions, that they receive from time to time. For instance, when there is a SYNCITY Urban Lab or a town hall meeting and a certain topic is discussed, users can quickly be asked one to three questions about their opinion, their ideas i.e. 6. Which gains in knowledge can Kju:Ti provide? Kju:Ti is intended to collect informal knowledge, to connect planning and research with the dwellers, and to substantiate the planning and research process with better data. By the deployment of a digital method and by its conception as low-threshold, complexity-reducing Tool, substantial participation hurdles can be addressed.")
        ],
      ),
    );
  }
}
