import 'package:flutter/material.dart';
import 'package:kjuti_client/util/mat_color/mat_color.dart';

class GradientCard extends StatelessWidget {

  final Widget childWidget;
  GradientCard({Key key, @required this.childWidget});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Center(
        child: Card(
          child: Container(
            decoration: BoxDecoration(
                border: new Border.all(
                    color: Colors.blueGrey,
                    width: 1.0,
                    style: BorderStyle.solid),
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      MatColor().create(Color.fromRGBO(240, 118, 56, 1)),
                      MatColor().create(Color.fromRGBO(240, 161, 87, 1))
                    ])),
            child: childWidget
          ),
        ),
      ),
    );
  }
}
