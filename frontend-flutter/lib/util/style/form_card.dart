import 'package:flutter/material.dart';

class FormCard extends StatelessWidget {
  final Widget childWidget;
  FormCard({Key key, @required this.childWidget});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
      child: Center(
        child: Card(
          child: Container(
              decoration: BoxDecoration(
                border: new Border.all(
                    color: Colors.blue,
                    width: 3.0,
                    style: BorderStyle.solid),
              ),
              child: childWidget),
        ),
      ),
    );
  }
}
