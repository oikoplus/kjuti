import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:kjuti_client/widgets/cam/camera.dart';
import 'package:kjuti_client/widgets/geo/geo.dart';
import 'package:kjuti_client/widgets/home/home.dart';

import '../app.dart';

@immutable
class AppRouter {
  static FluroRouter router = FluroRouter.appRouter;

  final List<AppRoute> _routes;
  final Handler _notFoundHandler;

  List<AppRoute> get routes => _routes;

  const AppRouter({
    @required List<AppRoute> routes,
    @required Handler notFoundHandler,
  })  : _routes = routes,
        _notFoundHandler = notFoundHandler;

  void setupRoutes() {
    router.notFoundHandler = _notFoundHandler;
    routes.forEach(
      (AppRoute route) => router.define(route.route, handler: route.handler),
    );
  }
}

class AppRoutes {
  static final routeNotFoundHandler = Handler(
      handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    debugPrint("Route not found. params: ${params.toString()}");
    return Home();
  });

  // static final root = AppRoute('/', Handler(
  //     handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  //   return App();
  // }), transitionType: TransitionType.fadeIn);
  static final home = AppRoute('/home', Handler(
      handlerFunc: (BuildContext context, Map<String, List<String>> params) {
    return App();
  }), transitionType: TransitionType.fadeIn);

  static final camRoute = AppRoute('/cam/:id',
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return CameraPage(qHash: params["id"][0]);
  }), transitionType: TransitionType.nativeModal);

  static final geoRoute = AppRoute('/geo/:id',
      Handler(handlerFunc: (BuildContext context, Map<String, dynamic> params) {
    return GeoPage(qHash: params["id"][0]);
  }), transitionType: TransitionType.nativeModal);

  static final List<AppRoute> routes = [home, geoRoute, camRoute];
}
