class ProfileNotFoundException implements Exception {
  String _message;
  ProfileNotFoundException(
      [String message = 'Profile not found']) {
    this._message = message;
  }

  @override
  String toString() {
    return _message;
  }
}
