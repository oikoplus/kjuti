import 'dart:async';

import 'package:firebase/firebase.dart' as firebase;



class FBMessaging {
  FBMessaging._();
  static FBMessaging _instance = FBMessaging._();

  static FBMessaging get instance => _instance;
  firebase.Messaging _mc;
  String _token;

  final _controller = StreamController<Map<String, dynamic>>.broadcast();

  Stream<Map<String, dynamic>> get stream => _controller.stream;

  void close() {
    _controller?.close();
  }

  Future<void> init() async {
    if (firebase.apps?.length == 0) {
      firebase.initializeApp(
          apiKey: 'AIzaSyDp-6gCyDAoWbMt7GQ91yNmcPF8NszxxOU',
          authDomain: 'kjuti-dev.firebaseapp.com',
          databaseURL: 'https://kjuti-dev.firebaseio.com',
          projectId: 'kjuti-dev',
          storageBucket: 'kjuti-dev.appspot.com',
          messagingSenderId: '844100693545',
          appId: '1:844100693545:web:8b28c41b009f1700edb638',
          measurementId: 'G-QCZTGM3GKT');
      _mc = firebase.messaging();
      _mc.usePublicVapidKey(
          'BDKKiGmT-zkCy8xibAQ4oFIHdiMowjKVWqEnq6cbkO1Y507EWrcKWJTvIoHhj6OPKMseRsP4Ys3KWVO-LG6OHto');
    }

    firebase.messaging().onMessage?.listen((event) {
      print('>p');
      _controller.add(event.data);
    });
  }

  Future requestPermission() {
    return _mc?.requestPermission();
  }

  Future<String> getToken([bool force = false]) async {
    if (force || _token == null) {
      await requestPermission();
      _token = await _mc.getToken();
    }
    return _token;
  }
}
