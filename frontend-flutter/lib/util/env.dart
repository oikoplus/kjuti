import 'dart:html' as html;
import 'dart:html';

class Env {
  // ignore: missing_return
  String deployPath() {}
  // ignore: missing_return
  String apiPath() {}
  // ignore: missing_return
  bool isDev() {}
  // ignore: missing_return
  bool isDevServer() {}
}

class EnvDev implements Env {
  String deployPath() {
    return 'http://localhost/';
  }
  String apiPath() {
    return 'http://localhost:8080/api';
  }
  bool isDev() {
    return true;
  }
  bool isDevServer() {
    return true;
  }
}
class EnvProd implements Env {
  String deployPath() {
    return window.location.href.split('#/')[0];
  }
  String apiPath() {
    return deployPath()+'api';
  }
  bool isDev() {
    return false;
  }
  bool isDevServer() {
    return false;
  }
}

Env env() {
  final url = html.window.location.href;
    if (url.startsWith('http://localhost')) {
      return new EnvDev();
    }
    return new EnvProd();
}
