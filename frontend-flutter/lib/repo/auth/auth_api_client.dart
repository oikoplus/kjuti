import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/util/env.dart';
import 'package:kjuti_client/util/exception/exceptions.dart';
import 'package:meta/meta.dart';

class AuthApiClient {
  final httpClient;

  AuthApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future register(Profile profile) async {
    final url = env().apiPath() + '/clients';

    Response response = await http.post(url,
        body: jsonEncode(profile),
        headers: {'Content-Type': 'application/json'});

    if (response.statusCode == 200) {
      return Profile.fromJson(ObjectResponse
          .fromJson(json.decode(response.body))
          .payload);
    } else {
      throw new GeneralException();
    }
  }

  Future<Profile> loadProfile(String fingerprint) async {
    final url = env().apiPath() + '/clients/$fingerprint';

    final Response response = await http.get(url);

    if (response.statusCode == 200) {
      Profile profile = Profile.fromJson(
          ObjectResponse.fromJson(json.decode(response.body)).payload);
      return profile;
    } else if (response.statusCode == 404) {
      throw new ProfileNotFoundException();
    }
    throw new GeneralException();
  }
}
