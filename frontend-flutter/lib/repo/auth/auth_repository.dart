import 'dart:async';

import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/repo/repositories.dart';
import 'package:meta/meta.dart';

class AuthRepository {
  final AuthApiClient authApiClient;

  AuthRepository({@required this.authApiClient})
      : assert(authApiClient != null);

  Future<Profile> loadProfile(String fingerprint) async {
    return await authApiClient.loadProfile(fingerprint);
  }

  Future register(Profile profile) async {
    return await authApiClient.register(profile);
  }
}
