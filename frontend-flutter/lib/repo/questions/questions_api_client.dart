import 'dart:convert';

import 'package:http/http.dart';
import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/util/env.dart';
import 'package:meta/meta.dart';

class QuestionsApiClient {
  final httpClient;

  QuestionsApiClient({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<QuestionList> get({String fingerprint}) async {
    var url = '${env().apiPath()}/o/';

    if (fingerprint.isNotEmpty) {
      url += '$fingerprint';
    } else {}
    final Response response = await httpClient.get(url);

    if (response.statusCode == 200) {
      List<Question> questions = (json.decode(response.body) as List)
          .map((i) => Question.fromJson(i))
          .toList();

      return QuestionList(questions: questions);
    } else {
      throw Exception(
          'Could not access api, status: ' + response.statusCode.toString());
    }
  }

  Future<Question> getTreeByHash({String qHash}) async {
    var url = '${env().apiPath()}/question/$qHash';

    final Response response = await httpClient.get(url);

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception(
          'Could not access api, status: ' + response.statusCode.toString());
    }
  }
}
