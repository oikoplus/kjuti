import 'dart:async';

import 'package:kjuti_client/models/models.dart';
import 'package:kjuti_client/repo/repositories.dart';
import 'package:meta/meta.dart';

class QuestionsRepository {
  final QuestionsApiClient apiClient;

  QuestionsRepository({@required this.apiClient}) : assert(apiClient != null);

  Future<QuestionList> getAllOpenTrees(String fingerprint) async {
    return await apiClient.get(fingerprint: fingerprint);
  }

  Future<Question> getTreeByHash(String qHash) async {
    return await apiClient.getTreeByHash(qHash: qHash);
  }
}
