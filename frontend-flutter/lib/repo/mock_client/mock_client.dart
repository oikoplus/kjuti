
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:kjuti_client/repo/mock_client/mock_data.dart';


final int bodyMaxPrintLength = 500;

MockClient mockClient = MockClient((request) async {
  final url = request.url.toString();
  Response response = Response('{}', 404);

  switch (url) {
    case 'http://localhost/ucabct/app/api/status':
      response = Response('{}', 200);
      break;
    case 'http://localhost/ucabct/app/api/contracts':
      String body = mock_contracts.toString();
      // String body = "[]";
      response = Response(body, 200);
      break;
  }

  String body = response.body;
  if (body.length > bodyMaxPrintLength) {
    body = body.toString().replaceRange(bodyMaxPrintLength, body.length, '...\n...\n}');
  }

  print(
      'MOCK> url: $url \nMOCK> code: ${response.statusCode}\nMOCK> body: $body');

  return response;
});
