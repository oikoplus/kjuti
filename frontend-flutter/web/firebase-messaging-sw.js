

importScripts('https://www.gstatic.com/firebasejs/7.13.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.13.1/firebase-messaging.js');

// import {firebaseInitInfo} from "env";

const firebaseInitInfo ={
    name: 'kju:ti',
    apiKey: 'AIzaSyDp-6gCyDAoWbMt7GQ91yNmcPF8NszxxOU',
    authDomain: 'kjuti-dev.firebaseapp.com',
    databaseURL: 'https://kjuti-dev.firebaseio.com',
    projectId: 'kjuti-dev',
    storageBucket: 'kjuti-dev.appspot.com',
    messagingSenderId: '844100693545',
    appId: '1:844100693545:web:8b28c41b009f1700edb638',
    measurementId: 'G-QCZTGM3GKT'
}

firebase.initializeApp(firebaseInitInfo);


const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    const parsedJSON = JSON.parse(payload.data['jsonData']);
    const goToQuestionsAction = [{'title': 'Go to answers ->', 'action': 0}];
    console.log('----');
    console.log('SW: push received:');
    console.log(payload);
    console.log('----');
    // Notification
    const notificationTitle = payload.data.title || '';
    const notificationOptions = {
        icon: 'https://png2.cleanpng.com/sh/b3e7b47c1cb3219856b76a9f6a8a146d/L0KzQYm3UsA5N5J1j5H0aYP2gLBuTgBqa5xxfdY2Y4Xmhb7phgIucZR0hp9sdXP4fbPskr02aWhoeqttNEjldLWBUr43PGg3T6M9OEG4QYm4UsM1P2c6T6QELoDxd1==/kisspng-pickled-cucumber-icon-cucumber-5a7cb9d48bdd82.6472714815181234765729.png',
        body: notificationTitle ? payload.data.question: '',
        actions: parsedJSON?.actions?.length <= 2 ? parsedJSON.actions : goToQuestionsAction,
        data: payload.data,
    };
    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});

self.addEventListener('notificationclick', function (event) {
    // CLICK ON NOTIFICATION
    const type = event.notification.data.answerType;
    const action = event.action;
    switch (type) {
        // CHOICE QUESTION
        case 'CHOICE':
            console.log(action);
            if (action.length > 2) {
                goToUrl(event, '/');
            }
            if (action) {
                sendAnswer(event);
            } else {
                goToUrl(event, '/');
            }
            break;
        // FREE TEXT REQUEST
        case 'TEXT':
            if (action) {
                goToUrl(event, '/#/text');
            }
            break;
        // CAMERA REQUEST
        case 'CAM':
            if (action === '0') {
                setTimeout(event.notification.close(), 500);
                return;
            }
            if (event.notification.data.qHash) {
                goToUrl(event, '/#/cam?q=' + event.notification.data.QHash);
            } else {
                goToUrl(event, '/#/cam');
            }
            break;
    }
});

function goToUrl(event, url) {
// Android needs explicit close.
    event.notification.close();
    event.waitUntil(
        clients.matchAll({type: 'window'}).then(windowClients => {
            // Check if there is already a window/tab open with the target URL
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                // If so, just focus it.
                if (client.url === url && 'focus' in client) {
                    return client.focus();
                }
            }
            // If not, then open the target URL in a new window/tab.
            if (clients.openWindow) {
                return clients.openWindow(url);
            }
        })
    );
}

function sendAnswer(event) {
    const url = '/api/' + event.notification.data.answerHash + '/' + event.action;
    // const data = {
    //     action: event.action,
    //     userFingerprint: event.notification.data.fingerprint,
    // answerHash: event.notification.data.answerHash,
    // timestamp: Date.now().toString()
    // }
    const params = {
        method: 'POST'
    };
    fetch(url, params)
        .then(data => {
            console.log("data: " + data);
        })
        .then(res => {
            console.log("response: " + res)
        })
        .catch(error => {
            console.log("error: " + error)
        })
    setTimeout(event.notification.close(), 1000);
}
