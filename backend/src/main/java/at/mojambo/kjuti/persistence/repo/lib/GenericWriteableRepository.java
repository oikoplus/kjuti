package at.mojambo.kjuti.persistence.repo.lib;

import javax.persistence.EntityManager;

public interface GenericWriteableRepository<E> {
    EntityManager getEntityManager();

    default void update(E entity) {
        EntityManager em = this.getEntityManager();
        em.merge(entity);
        em.flush();
        em.refresh(entity);
    }

    default void create(E entity) {
        EntityManager em = this.getEntityManager();
        em.persist(entity);
        em.flush();
    }

    default void delete(E entity) {
        EntityManager em = this.getEntityManager();
        em.remove(entity);
        em.flush();
    }
}
