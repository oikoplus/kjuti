package at.mojambo.kjuti.persistence.repo.lib;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

@Transactional
public interface GenericSearchableRepository<E> {
    EntityManager getEntityManager();

    Class<E> getEntityClass();

    default Long count() {
        CriteriaBuilder qb = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<Long> cq = qb.createQuery(Long.class);
        cq.select(qb.count(cq.from(this.getEntityClass())));
        return (Long)this.getEntityManager().createQuery(cq).getSingleResult();
    }

    default <T> List<E> loadAll() {
        CriteriaBuilder cb = this.getEntityManager().getCriteriaBuilder();
        CriteriaQuery<E> q = cb.createQuery(this.getEntityClass());
        Root<E> c = q.from(this.getEntityClass());
        q.select(c);
        TypedQuery<E> query = this.getEntityManager().createQuery(q);
        List<E> results = query.getResultList();
        return (List)(Objects.isNull(results) ? new ArrayList() : results);
    }

    default <T> Optional<E> findById(T id) {
        EntityManager em = this.getEntityManager();
        E entity = em.find(this.getEntityClass(), id);
        return entity != null ? Optional.of(entity) : Optional.empty();
    }

    default <T> Optional<E> findSingleByField(String fieldName, T value, Class<T> valueClass) {
        List<E> listByField = this.findListByField(fieldName, value, valueClass);
        return listByField != null && listByField.size() > 0 ? Optional.of(listByField.get(0)) : Optional.empty();
    }

    default <T> List<E> findListByField(String fieldName, T value, Class<T> valueClass) {
        EntityManager em = this.getEntityManager();
        Class<E> entityClass = this.getEntityClass();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<E> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(root);
        ParameterExpression<T> params = criteriaBuilder.parameter(valueClass);
        criteriaQuery.where(criteriaBuilder.equal(root.get(fieldName), params));
        TypedQuery<E> query = em.createQuery(criteriaQuery);
        query.setParameter(params, value);
        List<E> queryResult = query.getResultList();
        return queryResult;
    }
}
