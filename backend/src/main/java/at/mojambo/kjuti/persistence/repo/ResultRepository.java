package at.mojambo.kjuti.persistence.repo;

import at.mojambo.kjuti.persistence.entity.ResultEntity;
import at.mojambo.kjuti.persistence.repo.lib.GenericSearchableRepository;
import at.mojambo.kjuti.persistence.repo.lib.GenericWriteableRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;


/**
 * @author Daniel Moser <mojambo@softwaregaertner.at>
 */

@Transactional
public class ResultRepository implements GenericWriteableRepository<ResultEntity>, GenericSearchableRepository<ResultEntity> {
    @PersistenceContext(unitName = "app_pu")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Class<ResultEntity> getEntityClass() {
        return ResultEntity.class;
    }

}
