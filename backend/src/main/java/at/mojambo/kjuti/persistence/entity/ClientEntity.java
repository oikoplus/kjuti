package at.mojambo.kjuti.persistence.entity;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.List;


@SuppressWarnings("unused")
@Entity
@Table(name = "clients", schema = "kjuti")
public class ClientEntity {

    @SequenceGenerator(
            name = "clients_id_gen",
            schema = "kjuti",
            sequenceName = "clients_id_gen",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "clients_id_gen")
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "fingerprint")
    private Long fingerprint;

    @Column(name = "nickname")
    private String nickname;

    @Column(name = "firebase_token")
    private String firebaseToken;

    @Column(name = "activation_date")
    private String activationDate;

//    @OneToMany(mappedBy = "client")
//    private List<InteractionEntity> interactions;

//    @OneToMany(mappedBy = "client")
//    private List<ResultEntity> results;

//    @ManyToMany(mappedBy = "subscribedClients")
//    List<TopicEntity> topics;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(Long fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(String activationDate) {
        this.activationDate = activationDate;
    }

//    public List<InteractionEntity> getInteractions() {
//        return interactions;
//    }
//
//    public void setInteractions(List<InteractionEntity> interactions) {
//        this.interactions = interactions;
//    }

//    public List<ResultEntity> getResults() {
//        return results;
//    }
//
//    public void setResults(List<ResultEntity> results) {
//        this.results = results;
//    }
//
//    public List<TopicEntity> getTopics() {
//        return topics;
//    }
//
//    public void setTopics(List<TopicEntity> topics) {
//        this.topics = topics;
//    }
}
