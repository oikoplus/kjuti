package at.mojambo.kjuti.persistence.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@SuppressWarnings("unused")
@Entity
@Table(name = "interactions", schema = "kjuti")
public class InteractionEntity {
    @SequenceGenerator(
            name = "interactions_id_gen",
            schema = "kjuti",
            sequenceName = "interactions_id_gen",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "interactions_id_gen")
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "tid")
    private Long tid;

    @Column(name = "client_id", insertable = false, updatable = false)
    private Long clientId;

    @Column(name = "qid")
    private Long qid;

    @Column(name = "current_hash")
    private String currentHash;

    @Column(name = "active")
    private boolean active;

    @Column(name = "finished")
    private boolean finished;

    @Column(name = "result_id", insertable = false, updatable = false)
    private Long resultId;

    @Column(name = "timestamp_start")
    private LocalDateTime timestampStart;
    @Column(name = "timestamp_finished")
    private LocalDateTime timestampFinished;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "result_id", referencedColumnName = "id")
    private ResultEntity result;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private ClientEntity client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getQid() {
        return qid;
    }

    public void setQid(Long qid) {
        this.qid = qid;
    }

    public String getCurrentHash() {
        return currentHash;
    }

    public void setCurrentHash(String currentHash) {
        this.currentHash = currentHash;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public Long getResultId() {
        return resultId;
    }

    public void setResultId(Long resultId) {
        this.resultId = resultId;
    }

    public LocalDateTime getTimestampStart() {
        return timestampStart;
    }

    public void setTimestampStart(LocalDateTime timestampStart) {
        this.timestampStart = timestampStart;
    }

    public LocalDateTime getTimestampFinished() {
        return timestampFinished;
    }

    public void setTimestampFinished(LocalDateTime timestampFinished) {
        this.timestampFinished = timestampFinished;
    }

    public ResultEntity getResult() {
        return result;
    }

    public void setResult(ResultEntity result) {
        this.result = result;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }
}
