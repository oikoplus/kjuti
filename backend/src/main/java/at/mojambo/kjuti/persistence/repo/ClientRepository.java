package at.mojambo.kjuti.persistence.repo;

import at.mojambo.kjuti.persistence.entity.ClientEntity;
import at.mojambo.kjuti.persistence.repo.lib.GenericSearchableRepository;
import at.mojambo.kjuti.persistence.repo.lib.GenericWriteableRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author Daniel Moser <mojambo@softwaregaertner.at>
 */

@Transactional
public class ClientRepository implements GenericWriteableRepository<ClientEntity>, GenericSearchableRepository<ClientEntity> {
    @PersistenceContext(unitName = "app_pu")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Class<ClientEntity> getEntityClass() {
        return ClientEntity.class;
    }

    public List<String> getAllKeys() {
        CriteriaBuilder cB = em.getCriteriaBuilder();
        CriteriaQuery<ClientEntity> q = cB.createQuery(ClientEntity.class);
        Root<ClientEntity> c = q.from(ClientEntity.class);
        q.select(c);
        TypedQuery<ClientEntity> query = em.createQuery(q);
        List<ClientEntity> results = query.getResultList();
        List<String> keyList = new ArrayList<>(Collections.emptyList());
        results.forEach(entry -> {
            keyList.add(entry.getFirebaseToken());
        });
        return keyList;
    }
}
