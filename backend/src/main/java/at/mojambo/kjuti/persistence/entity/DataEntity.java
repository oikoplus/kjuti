package at.mojambo.kjuti.persistence.entity;

import com.fasterxml.jackson.databind.JsonNode;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
@Table(name = "data_store", schema = "kjuti")
public class DataEntity {

  @SequenceGenerator(
          name = "data_store_id_gen",
          schema = "kjuti",
          sequenceName = "data_store_id_gen",
          allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "data_store_id_gen")
  @Id
  @Column(name = "id")
  private Long id;

  @Lob
//  @Type(type = "org.hibernate.type.PrimitiveByteArrayBlobType")
  @Column(name = "data")
  private byte[] data;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public byte[] getData() {
    return data;
  }

  public void setData(byte[] data) {
    this.data = data;
  }
}
