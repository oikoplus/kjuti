package at.mojambo.kjuti.persistence.entity;

import at.mojambo.kjuti.model.dto.PathDto;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@SuppressWarnings("unused")
@Entity
@Table(name = "results", schema = "kjuti")
public class ResultEntity {

    @SequenceGenerator(
            name = "results_id_gen",
            schema = "kjuti",
            sequenceName = "results_id_gen",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "results_id_gen")
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "client_id", insertable = false, updatable = false)
    private Long client_id;

    @Column(name = "tid")
    private Long tid;

    @Column(name = "path")
    @Type(type = "JsonBTypePath")
    private PathDto path;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @OneToOne(mappedBy = "result", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private InteractionEntity interaction;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private ClientEntity client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getClient_id() {
        return client_id;
    }

    public void setClient_id(Long client_id) {
        this.client_id = client_id;
    }

    public PathDto getPath() {
        return path;
    }

    public void setPath(PathDto path) {
        this.path = path;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public InteractionEntity getInteraction() {
        return interaction;
    }

    public void setInteraction(InteractionEntity interaction) {
        this.interaction = interaction;
    }
}
