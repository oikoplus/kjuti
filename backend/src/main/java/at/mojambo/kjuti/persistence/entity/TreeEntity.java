package at.mojambo.kjuti.persistence.entity;

import javax.persistence.*;
import java.util.List;

@SuppressWarnings("unused")
@Entity
@Table(name = "trees", schema = "kjuti")
public class TreeEntity {

    @SequenceGenerator(
            name = "trees_id_gen",
            schema = "kjuti",
            sequenceName = "trees_id_gen",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trees_id_gen")
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "entry_node_id", insertable = false, updatable = false)
    private Long entryNodeId;

    @ManyToOne
    @JoinColumn(name = "entry_node_id", referencedColumnName = "id")
    QNodeEntity qNode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getEntryNodeId() {
        return entryNodeId;
    }

    public void setEntryNodeId(Long entryNodeId) {
        this.entryNodeId = entryNodeId;
    }

    public QNodeEntity getqNode() {
        return qNode;
    }

    public void setqNode(QNodeEntity qNode) {
        this.qNode = qNode;
    }
}
