package at.mojambo.kjuti.persistence.repo;

import at.mojambo.kjuti.persistence.entity.InteractionEntity;
import at.mojambo.kjuti.persistence.repo.lib.GenericSearchableRepository;
import at.mojambo.kjuti.persistence.repo.lib.GenericWriteableRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;


/**
 * @author Daniel Moser <mojambo@softwaregaertner.at>
 */

@Transactional
public class InteractionRepository implements GenericWriteableRepository<InteractionEntity>, GenericSearchableRepository<InteractionEntity> {
    @PersistenceContext(unitName = "app_pu")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Class<InteractionEntity> getEntityClass() {
        return InteractionEntity.class;
    }

}
