package at.mojambo.kjuti.persistence.entity;

import at.mojambo.kjuti.model.enums.NotificationType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
@Entity
@Table(name = "q_nodes", schema = "kjuti")
public class QNodeEntity {

    @SequenceGenerator(
            name = "q_nodes_id_gen",
            schema = "kjuti",
            sequenceName = "q_nodes_id_gen",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "q_nodes_id_gen")
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    @Column(name = "index")
    private Long index;

    @Column(name = "hash")
    private String hash;

    @Column(name = "answer_type")
    private NotificationType answerType;

    @Column(name = "parent_id", insertable = false, updatable = false)
    private Long parentId;

    @ManyToOne()
    private QNodeEntity parent;
    @OneToMany(mappedBy="parent", fetch = FetchType.EAGER)
    private List<QNodeEntity> children = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public QNodeEntity getParent() {
        return parent;
    }

    public void setParent(QNodeEntity parent) {
        this.parent = parent;
    }

    public NotificationType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(NotificationType answerType) {
        this.answerType = answerType;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public List<QNodeEntity> getChildren() {
        return children;
    }

    public void setChildren(List<QNodeEntity> children) {
        this.children = children;
    }

    public void addToChildren(QNodeEntity child) {
        this.children.add(child);
    }
}
