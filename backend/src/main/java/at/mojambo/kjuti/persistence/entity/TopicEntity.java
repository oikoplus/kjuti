package at.mojambo.kjuti.persistence.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "topics", schema = "kjuti")
public class TopicEntity {
    @SequenceGenerator(
            name = "topics_id_gen",
            schema = "kjuti",
            sequenceName = "topics_id_gen",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "topics_id_gen")
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

//    @ManyToMany
//    @JoinTable(
//            name = "topics",
//            joinColumns = @JoinColumn(name = "id"),
//            inverseJoinColumns = @JoinColumn(name = "id"))
//    List<ClientEntity> subscribedClients;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public List<ClientEntity> getSubscribedClients() {
//        return subscribedClients;
//    }
//
//    public void setSubscribedClients(List<ClientEntity> subscribedClients) {
//        this.subscribedClients = subscribedClients;
//    }
}
