package at.mojambo.kjuti.service;

import at.mojambo.kjuti.model.messaging.NotificationData;
import at.mojambo.kjuti.persistence.entity.QNodeEntity;
import at.mojambo.kjuti.persistence.repo.QNodeRepository;
import org.apache.commons.beanutils.BeanUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.logging.Logger;

@Transactional
@RequestScoped
public class QNodeService {

  @Inject
  QNodeRepository repo;

  private static final Logger LOGGER = Logger.getLogger(ClientService.class.getName());

  public NotificationData getByQHash(String qHash) {
    Optional<QNodeEntity> entityOptional = repo.findSingleByField("hash", qHash, String.class);
    if (!entityOptional.isPresent()) {
      throw new NotFoundException();
    }
    NotificationData data = null;
    try {
      BeanUtils.copyProperties(data, entityOptional.get());
    } catch (IllegalAccessException | InvocationTargetException e) {
      LOGGER.severe("BeanUtils could not copy properties from entity to dto.");
      e.printStackTrace();
    }
    return data;
  }

}
