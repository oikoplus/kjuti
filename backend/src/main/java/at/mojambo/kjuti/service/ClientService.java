package at.mojambo.kjuti.service;

import at.mojambo.kjuti.model.dto.ClientDto;
import at.mojambo.kjuti.persistence.entity.ClientEntity;
import at.mojambo.kjuti.persistence.repo.ClientRepository;
import org.apache.commons.beanutils.BeanUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Transactional
@RequestScoped
public class ClientService {

    @Inject
    ClientRepository clientRepository;

    private static final Logger LOGGER = Logger.getLogger(ClientService.class.getName());

    public List<ClientDto> getAll() {
        List<ClientEntity> entityList = clientRepository.loadAll();
        if (entityList.isEmpty()) {
            return Collections.emptyList();
        }
        return entityList.stream().map(this::dtoFromEntity).collect(Collectors.toList());
    }
    public ClientEntity getClientByFingerprint(Long fingerprint) {
        Optional<ClientEntity> entityOptional = clientRepository.findSingleByField("fingerprint", fingerprint, Long.class);
        if (entityOptional.isEmpty()) {
            throw new NotFoundException();
        }
        return entityOptional.get();
    }
    public Object getClientDtoByFingerprint(Long fingerprint) {
        ClientEntity entity = getClientByFingerprint(fingerprint);
        return dtoFromEntity(entity);
    }

    public ClientDto getClientByNickname(String nickname) {
        Optional<ClientEntity> entityOptional = clientRepository.findSingleByField("nickname", nickname, String.class);
        if (!entityOptional.isPresent()) {
            throw new NotFoundException();
        }
        return this.dtoFromEntity(entityOptional.get());
    }

    public String getTokenById(Long id) {
        Optional<ClientEntity> entityOptional = clientRepository.findById(id);
        if(entityOptional.isEmpty()) {
            throw new NotFoundException("Could not find client with id "+id);
        }
        return entityOptional.get().getFirebaseToken();
    }

    @Transactional
    public ClientDto createClient(ClientDto dto) throws InvocationTargetException, IllegalAccessException {
            ClientEntity entity = new ClientEntity();
            BeanUtils.copyProperties(entity, dto);
            clientRepository.create(entity);
            return dto;
    }

    public boolean checkFingerprintKnown(Long fingerprint) {
        Object result = this.clientRepository.findSingleByField("fingerprint",fingerprint, Long.class).orElse(null);
        return result != null;
    }

    private ClientDto dtoFromEntity(ClientEntity entity) {
        ClientDto clientDto = new ClientDto();
        try {
            BeanUtils.copyProperties(clientDto, entity);
        } catch (IllegalAccessException | InvocationTargetException e) {
            LOGGER.severe("BeanUtils could not copy properties from entity to dto.");
            e.printStackTrace();
        }
        return clientDto;
    }
}
