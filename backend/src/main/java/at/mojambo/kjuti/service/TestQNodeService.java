package at.mojambo.kjuti.service;

import at.mojambo.kjuti.model.enums.NotificationType;
import at.mojambo.kjuti.model.messaging.NotificationAction;
import at.mojambo.kjuti.persistence.entity.QNodeEntity;
import at.mojambo.kjuti.persistence.entity.TreeEntity;
import at.mojambo.kjuti.persistence.repo.QNodeRepository;
import at.mojambo.kjuti.persistence.repo.TreeRepository;
import at.mojambo.kjuti.service.util.Kjutil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Transactional
@RequestScoped
public class TestQNodeService {
  @Inject
  QNodeRepository qNodeRepository;
  @Inject
  TreeRepository treeRepository;

  private static final Logger LOGGER = Logger.getLogger(TestQNodeService.class.getName());

  public QNodeEntity loadCamTestNodes(NotificationType type) {
    if (treeRepository.findSingleByField("name", "Test Tree: " + type.toString(), String.class).isPresent()) {
      LOGGER.info("TESTDATA ALLREADY LOADED INTO DB");
      return null;
    }
    LOGGER.info("LOADING TESTDATA INTO DB ...");

    QNodeEntity question = new QNodeEntity();
    question.setTitle("Hello Kju:Ti!");
    question.setText("Share your view! (repeating)");
    question.setAnswerType(NotificationType.CAM);
    question.setIndex(-1L);
    question.setHash(Kjutil.getHash(10));

    QNodeEntity end = new QNodeEntity();
    end.setTitle("Thank you for that!");
    end.setText("You are beautiful!");
    end.setIndex(-2L);
    end.setHash(Kjutil.getHash(10));
    end.setParent(question);

    qNodeRepository.create(question);
    qNodeRepository.create(end);

    return question;
  }
  public QNodeEntity loadGeoTestNodes(NotificationType type) {
    if (treeRepository.findSingleByField("name", "Test Tree: " + type.toString(), String.class).isPresent()) {
      LOGGER.info("TESTDATA ALLREADY LOADED INTO DB");
      return null;
    }
    LOGGER.info("LOADING TESTDATA INTO DB ...");

    QNodeEntity question = new QNodeEntity();
    question.setTitle("Hello Kju:Ti!");
    question.setText("Where do you feel especially relaxed? (repeating)");
    question.setAnswerType(NotificationType.GEO);
    question.setIndex(-1L);
    question.setHash(Kjutil.getHash(10));

    QNodeEntity end = new QNodeEntity();
    end.setTitle("Thank you for you answer!");
    end.setText("Seems to be very cozy there!");
    end.setIndex(-2L);
    end.setHash(Kjutil.getHash(10));
    end.setParent(question);

    qNodeRepository.create(question);
    qNodeRepository.create(end);

    return question;
  }


  @Transactional
  public void loadAllTestTrees() {
    QNodeEntity camEntryNode = loadCamTestNodes(NotificationType.CAM);
    TreeEntity camTreeEntity = new TreeEntity();
    camTreeEntity.setName("Test Tree: " + NotificationType.CAM);
    camTreeEntity.setDescription("Simple " + NotificationType.CAM + " testing.");
    camTreeEntity.setqNode(camEntryNode);
    treeRepository.create(camTreeEntity);

//    QNodeEntity textEntryNode = loadCamTestNodes(NotificationType.TEXT);
//    TreeEntity textTreeEntity = new TreeEntity();
//    textTreeEntity.setName("Test Tree: "+NotificationType.CAM);
//    textTreeEntity.setDescription("Simple " + NotificationType.CAM +" testing.");
//    textTreeEntity.setqNode(camEntryNode);
//    treeRepository.create(textTreeEntity);

//    QNodeEntity EntryNode = loadCamTestNodes(NotificationType.CAM);
//    TreeEntity TreeEntity = new TreeEntity();
//    TreeEntity.setName("Test Tree: "+NotificationType.CAM);
//    TreeEntity.setDescription("Simple " + NotificationType.CAM +" testing.");
//    TreeEntity.setqNode(camEntryNode);
//    treeRepository.create(TreeEntity);
  }

  @Transactional
  public void loadTestTree(NotificationType type) {
    QNodeEntity entryNode;
    switch (type) {
      case CAM:
        entryNode = loadCamTestNodes(type);
        break;
      case GEO:
        entryNode = loadGeoTestNodes(type);
        break;
      case CHOICE:
        entryNode = loadChoiceTestNodes();
        break;
      case TEXT:
        entryNode = loadTextTestNodes();
        break;
      default:
        return;
    }
    if (entryNode == null) {
      return;
    }

    TreeEntity treeEntity = new TreeEntity();
    treeEntity.setName("Test Tree: " + type.toString());
    treeEntity.setDescription("Simple " + type.toString() + " testing.");
    treeEntity.setqNode(entryNode);
    treeRepository.create(treeEntity);
  }

  private QNodeEntity loadTextTestNodes() {
    return null;
  }

  private QNodeEntity loadChoiceTestNodes() {
    return null;
  }

  public List<NotificationAction> getActionTextAndIndexFromChildrenIds(List<QNodeEntity> children) {
    List<NotificationAction> actionList = new ArrayList<>();
    children.forEach(child -> {
      NotificationAction action = new NotificationAction();
      action.setTitle(child.getText());
      action.setAction(child.getIndex());
      actionList.add(action);
    });
    return actionList;
  }

  public QNodeEntity getQNodeById(Long qid) {
    Optional<QNodeEntity> qNodeEntityOptional = qNodeRepository.findById(qid);
    if (qNodeEntityOptional.isEmpty()) {
      throw new NotFoundException("Could not find qnode for id " + qid);
    }
    return qNodeEntityOptional.get();
  }

  @Transactional
  public QNodeEntity getByParentId(Long id) {
    Optional<QNodeEntity> qNodeEntityOptional = qNodeRepository.findSingleByField("parentId", id, Long.class);
    if (qNodeEntityOptional.isEmpty()) {
      throw new NotFoundException("Could not find qnode for id " + id);
    }
    return qNodeEntityOptional.get();
  }
}

