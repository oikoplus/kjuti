package at.mojambo.kjuti.service;

import at.mojambo.kjuti.firebase.FireBaseService;
import at.mojambo.kjuti.persistence.entity.ClientEntity;
import at.mojambo.kjuti.persistence.entity.InteractionEntity;
import at.mojambo.kjuti.persistence.entity.QNodeEntity;
import org.apache.http.HttpResponse;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.logging.Logger;

@RequestScoped
@Transactional
public class PushService {
    @Inject
    FireBaseService fireBaseService;
    @Inject
    TestQNodeService testQNodeService;

    private static final Logger LOGGER = Logger.getLogger(PushService.class.getName());

    public boolean send(InteractionEntity interactionEntity, boolean isFollowup) {
        ClientEntity clientEntity = interactionEntity.getClient();
        QNodeEntity qNodeEntity = testQNodeService.getQNodeById(interactionEntity.getQid());
        HttpResponse response = null;
        try {
            response = fireBaseService.sendPush(clientEntity, qNodeEntity, interactionEntity.getCurrentHash(), isFollowup);
            
        } catch (Exception e) {
            LOGGER.severe("COULD NOT SEND PUSH to: " + clientEntity.getNickname());
            throw new WebApplicationException("Could not send push to: " + clientEntity.getNickname());
        }
        return true;
    }

    public void sayThankyou(ClientEntity client) {
        fireBaseService.sayThankyou(client);
    }

    public void ping(Long clientId) {
        fireBaseService.ping(clientId);
    }

    public void cam(Long clientId) {
        // TODO implement cam ping
    }
}
