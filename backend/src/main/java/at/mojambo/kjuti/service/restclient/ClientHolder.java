package at.mojambo.kjuti.service.restclient;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.logging.Logger;

@ApplicationScoped
public class ClientHolder {

    private static final Logger LOGGER = Logger.getLogger(ClientHolder.class.getName());

    private Client jaxRestClient;

    @PostConstruct
    public void init() {
        jaxRestClient = ClientBuilder.newClient();
    }

    @PreDestroy
    public void preDestroy() {
        jaxRestClient.close();
    }

    public Client getClient() {
        return this.jaxRestClient;
    }
}



