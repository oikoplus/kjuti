

package at.mojambo.kjuti.service;


import at.mojambo.kjuti.persistence.entity.DataEntity;
import at.mojambo.kjuti.persistence.repo.DataRepository;
import at.mojambo.kjuti.rest.model.TData;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.UnsupportedEncodingException;

@Transactional
public class DataService {

  @Inject
  DataRepository dataRepository;
  @Inject
  InteractionService interactionService;

  public void handleTextData(String ahash, @NotNull TData textData) {
    DataEntity dataEntity = new DataEntity();
    try {
      dataEntity.setData(textData.getTitle().getBytes("UTF-8"));
    } catch (UnsupportedEncodingException throwables) {
      throwables.printStackTrace();
    }
    dataRepository.create(dataEntity);
    interactionService.storeDataResult(dataEntity, ahash);

  }

}
