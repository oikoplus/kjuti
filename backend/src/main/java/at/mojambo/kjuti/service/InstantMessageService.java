package at.mojambo.kjuti.service;

import at.mojambo.kjuti.firebase.FireBaseService;
import at.mojambo.kjuti.model.dto.PathDto;
import at.mojambo.kjuti.model.dto.PathNodeDto;
import at.mojambo.kjuti.model.enums.NotificationType;
import at.mojambo.kjuti.model.messaging.NotificationData;
import at.mojambo.kjuti.persistence.entity.*;
import at.mojambo.kjuti.persistence.repo.ClientRepository;
import at.mojambo.kjuti.persistence.repo.InteractionRepository;
import at.mojambo.kjuti.persistence.repo.QNodeRepository;
import at.mojambo.kjuti.persistence.repo.TreeRepository;
import at.mojambo.kjuti.service.util.Kjutil;
import org.apache.http.HttpResponse;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Transactional
@RequestScoped
public class InstantMessageService {

  @Inject
  TreeRepository treeRepository;
  @Inject
  QNodeRepository qNodeRepository;
  @Inject
  ClientRepository clientRepository;
  @Inject
  FireBaseService fireBaseService;

  private static final Logger LOGGER = Logger.getLogger(InstantMessageService.class.getName());

  private Random random = new Random();

  @Transactional
  public Object sendMorningMessage() {
    if (treeRepository.findSingleByField("name", "Tuesday morning", String.class).isPresent()) {
      LOGGER.info("TESTDATA FOR Tuesday morning ALLREADY LOADED INTO DB");
      return null;
    }
    LOGGER.info("LOADING Tuesday morning TESTDATA INTO DB ...");

    QNodeEntity question = new QNodeEntity();
    question.setTitle("🌅 Kju:Te morning!");
    question.setText("Did you have a good night of 😴?");
    question.setAnswerType(NotificationType.CHOICE);
    question.setIndex(-1L);
    question.setHash(Kjutil.getHash(10));

    // no
    QNodeEntity answer_0 = new QNodeEntity();
    answer_0.setText("🥱🦥" );
    answer_0.setIndex(0L);
    answer_0.setHash(Kjutil.getHash(10));
    answer_0.setParent(question);

    //yes
    QNodeEntity answer_1 = new QNodeEntity();
    answer_1.setText("😃🙌");
    answer_1.setIndex(1L);
    answer_1.setHash(Kjutil.getHash(10));
    answer_1.setParent(question);

    // no followup!
    QNodeEntity end_0 = new QNodeEntity();
    end_0.setTitle("🌞 Rise and shine!");
    end_0.setText("Grab some ☕, see you soon 💪!");
    end_0.setIndex(-2L);
    end_0.setHash(Kjutil.getHash(10));
    end_0.setParent(answer_0);

    QNodeEntity end_1 = new QNodeEntity();
    end_1.setTitle("🌞 Rise and shine!");
    end_1.setText("See you soon then, and stay 🧊!");
    end_1.setIndex(-2L);
    end_1.setHash(Kjutil.getHash(10));
    end_1.setParent(answer_1);

    qNodeRepository.create(answer_0);
    qNodeRepository.create(answer_1);
    qNodeRepository.create(end_0);
    qNodeRepository.create(end_1);
    qNodeRepository.create(question);

    TreeEntity morningTree = new TreeEntity();
    morningTree.setName("Morning Tree");
    morningTree.setDescription("Good morning baby.");
    morningTree.setqNode(question);
    treeRepository.create(morningTree);
    return question;
  }


  public HttpResponse sendToSingleClient(Long clientId, String text) {
    Optional<ClientEntity> clientEntityOptional = clientRepository.findById(clientId);
    if (clientEntityOptional.isEmpty()) {
      throw new NotFoundException("Could not fine client with id " + clientId);
    }
    return fireBaseService.sendSimplePush(clientEntityOptional.get(), text);
  }

  public HttpResponse sendToAllClients(String text) {
    List<ClientEntity> allClients = clientRepository.loadAll();
    allClients.forEach(clientEntity -> {
      fireBaseService.sendSimplePush(clientEntity, text);
    });
    return null;
  }
}
