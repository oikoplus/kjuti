package at.mojambo.kjuti.service;

import at.mojambo.kjuti.persistence.entity.InteractionEntity;
import at.mojambo.kjuti.persistence.entity.TreeEntity;
import at.mojambo.kjuti.persistence.repo.InteractionRepository;
import at.mojambo.kjuti.persistence.repo.TreeRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Transactional
@RequestScoped
public class TreeService {

    @Inject
    TreeRepository treeRepository;
    @Inject
    InteractionRepository interactionRepository;
    @Inject
    InteractionService interactionService;
    @Inject
    PushService pushService;

    private static final Logger LOGGER = Logger.getLogger(TreeService.class.getName());

    public List<InteractionEntity> setupInteractionsForTreeById(Long id, Long clientId) {
        Optional<TreeEntity> optionalTreeEntity = treeRepository.findById(id);
        if (optionalTreeEntity.isEmpty()) {
            throw new NotFoundException();
        }
        TreeEntity treeEntity = optionalTreeEntity.get();
        return interactionService.createEntriesForNewTree(treeEntity, clientId);
    }

    public List<String> startTreeById(long id, long userId) {
        List<InteractionEntity> openInactiveList = interactionRepository.findListByField("tid", id, Long.class).stream()
                .filter(entity -> !entity.isActive())
                .filter(entity -> !entity.isFinished())
                .collect(Collectors.toList());

        List<String> sentToList = new ArrayList<>();
        if (userId >= 0) {
            for (InteractionEntity interaction : openInactiveList) {
                if (this.pushService.send(interaction, false)) {
                    interaction.setActive(true);
                    interaction.setTimestampStart(LocalDateTime.now());
                }
            }
        } else {
            for (InteractionEntity interaction : openInactiveList) {
                if (interaction.getClientId() == userId && this.pushService.send(interaction, false)) {
                    interaction.setActive(true);
                    interaction.setTimestampStart(LocalDateTime.now());
                }
            }
        }
        return sentToList;
    }
}

