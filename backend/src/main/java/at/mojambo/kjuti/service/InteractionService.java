package at.mojambo.kjuti.service;

import at.mojambo.kjuti.firebase.FireBaseService;
import at.mojambo.kjuti.model.dto.PathDto;
import at.mojambo.kjuti.model.dto.PathNodeDto;
import at.mojambo.kjuti.model.messaging.NotificationData;
import at.mojambo.kjuti.persistence.entity.*;
import at.mojambo.kjuti.persistence.repo.ClientRepository;
import at.mojambo.kjuti.persistence.repo.InteractionRepository;
import at.mojambo.kjuti.service.util.Kjutil;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

import static java.util.Objects.isNull;

@Transactional
@RequestScoped
public class InteractionService {

  @Inject
  InteractionRepository interactionRepository;
  @Inject
  ClientRepository clientRepository;
  @Inject
  ClientService clientService;
  @Inject
  TestQNodeService testQNodeService;
  @Inject
  ResultService resultService;
  @Inject
  PushService pushService;
  @Inject
  FireBaseService fireBaseService;
  @Inject
  TreeService treeService;

  private static final Logger LOGGER = Logger.getLogger(InteractionService.class.getName());

  public List<InteractionEntity> createEntriesForNewTree(TreeEntity treeEntity, Long clientId) {
    List<InteractionEntity> newInteractionsEntries = new ArrayList<>();

    ArrayList<ClientEntity> newInteractionClients = new ArrayList<>();
    if (clientId >= 0) {
      Optional<ClientEntity> entityOptinal = clientRepository.findById(clientId);
      if (entityOptinal.isEmpty()) {
        throw new NotFoundException();
      }
      newInteractionClients.add(entityOptinal.get());
    } else {
      newInteractionClients = new ArrayList<>(clientRepository.loadAll());
    }

    LOGGER.info("###### loaded clients: ");
    newInteractionClients.forEach(client -> {
      LOGGER.info(client.getNickname());
    });
    LOGGER.info("###### loaded clients: ");
    for (ClientEntity client : newInteractionClients) {
      InteractionEntity entity = new InteractionEntity();
      entity.setClient(client);
      entity.setQid(treeEntity.getqNode().getId());
      entity.setTid(treeEntity.getId());
      entity.setActive(false);
      entity.setFinished(false);
      entity.setCurrentHash(Kjutil.getHash(256));
      ResultEntity resultEntity = resultService.createNewFromInteraction(entity);
      entity.setResult(resultEntity);
      newInteractionsEntries.add(entity);
    }
    newInteractionsEntries.forEach(entity -> interactionRepository.create(entity));
    return newInteractionsEntries;
  }

  private boolean hasRunningOrFinishedInteractionOnTree(Long clientId, Long treeId) {
    for (InteractionEntity interaction : interactionRepository.findListByField("clientId", clientId, Long.class)) {
      if (interaction.getTid().equals(treeId)) {
        return true;
      }
    }
    return false;
  }

  public NotificationData getNotificationDataByHash(String hash) {
    return fireBaseService.generateNotificationDataFromInteraction(getByHash(hash));
  }

  public List<NotificationData> getAllOpen(Long fingerprint) {
    ClientEntity clientEntity = clientService.getClientByFingerprint(fingerprint);
    // find open by FP
    List<NotificationData> result = new ArrayList<>();
    List<InteractionEntity> list = interactionRepository.findListByField("clientId", clientEntity.getId(), Long.class);
    for (InteractionEntity interactionEntity : list) {
      if (!interactionEntity.isFinished()) {
        fireBaseService.generateNotificationDataFromInteraction(interactionEntity);
        result.add(fireBaseService.generateNotificationDataFromInteraction(interactionEntity));
      }
    }
    return result;
  }

  @Transactional
  public void handleAnswer(String hash, String index) {
    InteractionEntity interactionEntity = getByHash(hash);
    interactionEntity.setCurrentHash("");

    QNodeEntity nextNodeOrNull = storeResultsAndCheckForNewQNode(interactionEntity, index);
    if (isNull(nextNodeOrNull) || Objects.requireNonNull(nextNodeOrNull).getIndex() < 0) {
      // finish this interaction
      interactionEntity.setFinished(true);
      interactionEntity.setActive(false);
      interactionEntity.setTimestampFinished(LocalDateTime.now());
      interactionEntity.setCurrentHash(Kjutil.getHash(256));
      interactionRepository.update(interactionEntity);
      pushService.sayThankyou(interactionEntity.getClient());
    } else {
      // continue this interaction
      interactionEntity.setCurrentHash(Kjutil.getHash(256));

      // has continuing children && is not last
      if (null == nextNodeOrNull.getAnswerType() || nextNodeOrNull.getIndex() >= 0) {
        QNodeEntity followup = testQNodeService.getByParentId(nextNodeOrNull.getId());
        interactionEntity.setQid(followup.getId());
        pushService.send(interactionEntity, true);
        return;
      }
      interactionEntity.setQid(nextNodeOrNull.getId());
      pushService.send(interactionEntity, true);
    }
  }


  public void handleAnswerFromApp(String hash, String index) {
    InteractionEntity interactionEntity = getByHash(hash);
    interactionEntity.setCurrentHash("");
    QNodeEntity nextNodeOrNull = storeResultsAndCheckForNewQNode(interactionEntity, index);

  }

  private QNodeEntity storeResultsAndCheckForNewQNode(InteractionEntity interactionEntity, String index) {
    // get or create results
    ResultEntity resultEntity = interactionEntity.getResult();
    QNodeEntity qNodeEntity = testQNodeService.getQNodeById(interactionEntity.getQid());
    ClientEntity clientEntity = interactionEntity.getClient();

    PathDto pathDto = resultEntity.getPath();
    // if first in resultpath, set qid of interaction
    pathDto.checkFirst(interactionEntity.getQid());

    QNodeEntity newQNode = null;
    for (QNodeEntity child : qNodeEntity.getChildren()) {
      if (child.getIndex() == Long.parseLong(index)) {
        LOGGER.info("<<<<< received from " + clientEntity.getNickname() + ", index: " + index + ", answer: " + child.getText());
        resultEntity.setTimestamp(LocalDateTime.now());
        PathNodeDto pathNode = new PathNodeDto();
        pathNode.setQid(child.getId());
        pathNode.setIndex(child.getIndex());
        pathDto.addToPath(pathNode);
        if (!child.getChildren().isEmpty() && !areTerminatingChildren(child.getChildren())) {
          newQNode = child;
        }
      }
    }
    return newQNode;
  }

  public void storeDataResult(DataEntity dataEntity, String hash) {
    InteractionEntity interactionEntity = getByHash(hash);
    ResultEntity resultEntity = interactionEntity.getResult();

    PathDto pathDto = resultEntity.getPath();
    PathNodeDto dataNode = new PathNodeDto();
    dataNode.setDataId(dataEntity.getId());
    dataNode.setQid(interactionEntity.getQid());
    pathDto.addToPath(dataNode);
    interactionEntity.setActive(false);
    interactionEntity.setFinished(true);
    interactionEntity.setTimestampFinished(LocalDateTime.now());

  }

  private boolean areTerminatingChildren(List<QNodeEntity> children) {
    for (QNodeEntity child : children) {
      if (child.getIndex() < 0) {
        return true;
      }
    }
    return false;
  }

  public InteractionEntity getByHash(String hash) {
    Optional<InteractionEntity> optionalInteractionEntity = interactionRepository.findSingleByField("currentHash", hash, String.class);
    if (optionalInteractionEntity.isEmpty()) {
      throw new NotFoundException("Could not find open interaction for hash " + hash);
    }
    return optionalInteractionEntity.get();
  }


  public boolean reset(Long fingerprint) {
    ClientEntity clientEntity = clientService.getClientByFingerprint(fingerprint);
    if (clientEntity.getId().equals(128L)) {
      return true;
    }
    return false;
  }
}

