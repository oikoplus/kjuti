package at.mojambo.kjuti.service;

import at.mojambo.kjuti.model.dto.PathDto;
import at.mojambo.kjuti.persistence.entity.InteractionEntity;
import at.mojambo.kjuti.persistence.entity.ResultEntity;
import at.mojambo.kjuti.persistence.repo.ResultRepository;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import java.util.List;
import java.util.logging.Logger;

@Transactional
@RequestScoped
public class ResultService {

    @Inject
    ResultRepository resultRepository;

    private static final Logger LOGGER = Logger.getLogger(ResultService.class.getName());

    public ResultEntity createNewFromInteraction(InteractionEntity entity) {
        ResultEntity resultEntity = new ResultEntity();
        resultEntity.setClient(entity.getClient());
        resultEntity.setTid(entity.getTid());
        resultEntity.setPath(new PathDto());
        resultRepository.create(resultEntity);
        return  resultEntity;
    }

}

