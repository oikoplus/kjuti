package at.mojambo.kjuti.model.messaging;

public class Answer {
    private String action;
    private Long userFingerprint;
    private String answerHash;
    private String timestamp;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getUserFingerprint() {
        return userFingerprint;
    }
    public void setUserFingerprint(Long userFingerprint) {
        this.userFingerprint = userFingerprint;
    }
    public String getAnswerHash() {
        return answerHash;
    }
    public void setAnswerHash(String answerHash) {
        this.answerHash = answerHash;
    }
    public String getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
