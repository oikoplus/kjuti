package at.mojambo.kjuti.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QNode implements Serializable {
    private String title;
    private String text;
    private Long index;
    private String hash;
    private List<QNode> children = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public List<QNode> getChildren() {
        return children;
    }

    public void setChildren(List<QNode> children) {
        this.children = children;
    }

    public void addToChildren(QNode child) {
        this.children.add(child);
    }
}
