package at.mojambo.kjuti.model.dto;

import java.io.Serializable;

public class PathNodeDto implements Serializable  {
    private Long qid;
    private Long index;
    private Long dataId;

    public PathNodeDto(Long qid, Long index) {
        this.qid = qid;
        this.index = index;
    }

    public PathNodeDto() {
    }

    public Long getQid() {
        return qid;
    }

    public void setQid(Long qid) {
        this.qid = qid;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }
}
