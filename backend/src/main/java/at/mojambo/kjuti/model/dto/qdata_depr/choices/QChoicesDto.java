package at.mojambo.kjuti.model.dto.qdata_depr.choices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QChoicesDto implements Serializable {

    private String question;

    private List<Choice> choices = new ArrayList<>();

    private String qHash;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public void addChoice(Choice choice) {
        this.choices.add(choice);
    }

    public String getqHash() {
        return qHash;
    }

    public void setqHash(String qHash) {
        this.qHash = qHash;
    }
}
