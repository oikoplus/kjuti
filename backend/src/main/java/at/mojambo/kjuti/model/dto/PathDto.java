package at.mojambo.kjuti.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PathDto  implements Serializable {
    private List<PathNodeDto> path = new ArrayList<>();

    public List<PathNodeDto> getPath() {
        return path;
    }

    public void setPath(List<PathNodeDto> path) {
        this.path = path;
    }

    public void addToPath(PathNodeDto pathNode) {
        path.add(pathNode);
    }

    public void checkFirst(Long qid) {
        if (path.isEmpty()) {
            path.add(new PathNodeDto(qid, -1L));
        }
    }
}
