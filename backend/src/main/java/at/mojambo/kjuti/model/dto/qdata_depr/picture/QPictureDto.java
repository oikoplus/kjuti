package at.mojambo.kjuti.model.dto.qdata_depr.picture;



import java.io.Serializable;

public class QPictureDto  implements Serializable {
    private boolean frontCam = false;

    public QPictureDto(String question, boolean frontCam) {

        this.frontCam = frontCam;
    }


    public boolean isFrontCam() {
        return frontCam;
    }

    public void setFrontCam(boolean frontCam) {
        this.frontCam = frontCam;
    }
}
