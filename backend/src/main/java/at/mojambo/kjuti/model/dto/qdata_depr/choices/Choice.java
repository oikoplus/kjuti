package at.mojambo.kjuti.model.dto.qdata_depr.choices;

import java.io.Serializable;

public class Choice implements Serializable {
    private String label;
    private boolean value = false;
    private String aHash;

    public Choice(String label, boolean value) {
        this.label = label;
        this.value = value;
    }
    public Choice(String label, String aHash) {
        this.label = label;
        this.aHash = aHash;
    }
    public Choice() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getaHash() {
        return aHash;
    }

    public void setaHash(String aHash) {
        this.aHash = aHash;
    }
}
