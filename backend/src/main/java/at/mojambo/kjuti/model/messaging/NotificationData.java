package at.mojambo.kjuti.model.messaging;

import at.mojambo.kjuti.model.enums.NotificationType;

public class NotificationData {
    private String title;
    private NotificationType answerType;
    private String question;
    private String QHash;
    private String answerHash = "";
    private Boolean isFollowUp = false;
    private String jsonData;

    public NotificationData(String title, NotificationType type) {
        this.title = title;
        this.answerType = type;
    }
    public NotificationData(String title) {
        this.title = title;
    }
    public NotificationData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQHash() {
        return QHash;
    }

    public void setQHash(String QHash) {
        this.QHash = QHash;
    }

    public String getAnswerHash() {
        return answerHash;
    }

    public void setAnswerHash(String answerHash) {
        this.answerHash = answerHash;
    }

    public Boolean getFollowUp() {
        return isFollowUp;
    }

    public void setFollowUp(Boolean followUp) {
        isFollowUp = followUp;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public NotificationType getAnswerType() {
        return answerType;
    }

    public void setAnswerType(NotificationType type) {
        this.answerType = type;
    }
}
