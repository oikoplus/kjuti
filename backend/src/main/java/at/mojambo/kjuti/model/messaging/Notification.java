package at.mojambo.kjuti.model.messaging;

public class Notification {
    private NotificationData data;
    private String to;

    public Notification() {
    }

    public Notification(String to) {
        this.to = to;
    }

    public NotificationData getData() {
        return data;
    }

    public void setData(NotificationData data) {
        this.data = data;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
