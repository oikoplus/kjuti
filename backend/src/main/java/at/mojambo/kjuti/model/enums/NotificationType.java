package at.mojambo.kjuti.model.enums;

public enum NotificationType {
  CHOICE,
  TEXT,
  CAM,
  GEO,
  NONE;
}
