package at.mojambo.kjuti.model.enums;

public enum QuestionType {
    CAM_FRONT,
    CAM_BACK,
    CHOICE_BIN,
    CHOICE_MULTI;
}
