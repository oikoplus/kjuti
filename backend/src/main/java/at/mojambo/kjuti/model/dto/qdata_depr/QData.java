package at.mojambo.kjuti.model.dto.qdata_depr;

import com.fasterxml.jackson.databind.JsonNode;

public abstract class QData {

    private String question;

    private JsonNode data;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }
}
