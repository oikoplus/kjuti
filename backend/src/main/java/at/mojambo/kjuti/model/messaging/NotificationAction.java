package at.mojambo.kjuti.model.messaging;

public class NotificationAction {
    private String title;
    private Long action;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getAction() {
        return action;
    }

    public void setAction(Long action) {
        this.action = action;
    }
}
