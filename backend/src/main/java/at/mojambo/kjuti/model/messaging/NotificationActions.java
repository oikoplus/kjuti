package at.mojambo.kjuti.model.messaging;

import java.util.ArrayList;
import java.util.List;

public class NotificationActions {
    private List<NotificationAction> actions = new ArrayList<>();

    public NotificationActions() {
    }

    public NotificationActions(List<NotificationAction> actions) {
        this.actions = actions;
    }

    public List<NotificationAction> getActions() {
        return actions;
    }

    public void setActions(List<NotificationAction> actions) {
        this.actions = actions;
    }
}
