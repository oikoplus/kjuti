package at.mojambo.kjuti.model.dto;

import java.util.List;

public class UserListDto {
    private List<ClientDto> clientDtos;

    public List<ClientDto> getUsers() {
        return clientDtos;
    }

    public void setUsers(List<ClientDto> clientDtos) {
        this.clientDtos = clientDtos;
    }
}
