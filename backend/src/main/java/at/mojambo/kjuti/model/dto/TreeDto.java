package at.mojambo.kjuti.model.dto;

import java.io.Serializable;

public class TreeDto implements Serializable {
    private Long id;
    private String name;
    private String description;
    private Long entryNodeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getEntryNodeId() {
        return entryNodeId;
    }

    public void setEntryNodeId(Long entryNodeId) {
        this.entryNodeId = entryNodeId;
    }
}
