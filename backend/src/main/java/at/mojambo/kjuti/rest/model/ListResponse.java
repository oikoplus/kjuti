package at.mojambo.kjuti.rest.model;
import java.util.ArrayList;
import java.util.List;

public class ListResponse<T> {
    private List<T> payload = new ArrayList();

    public ListResponse() {
    }

    public List<T> getPayload() {
        return this.payload;
    }

    public static class Builder {
        private final ListResponse response = new ListResponse();

        public Builder() {
        }

        public <T> ListResponse.Builder payload(List<T> data) {
            this.response.getPayload().addAll(data);
            return this;
        }

        public <T> ListResponse<T> build() {
            return this.response;
        }
    }
}
