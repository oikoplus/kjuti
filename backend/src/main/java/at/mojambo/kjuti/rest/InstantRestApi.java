package at.mojambo.kjuti.rest;

import at.mojambo.kjuti.service.InstantMessageService;
import com.google.firebase.database.annotations.NotNull;
import org.apache.http.HttpResponse;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Daniel Moser <office@mojambo.at>
 */

@Path("/instant")
public class InstantRestApi {

    @Inject
    InstantMessageService service;

    @POST
    @Path("/morning")
    @Transactional
    @Produces({APPLICATION_JSON})
    public Response createMorningTree() {
        return Response.ok(service.sendMorningMessage()).build();
    }

    @POST
    @Path("/{clientId}/{text}")
    @Produces({APPLICATION_JSON})
    public HttpResponse sendToSingleClient(@NotNull @PathParam("clientId") Long clientId, @NotNull  @PathParam("text") String text) {
        return service.sendToSingleClient(clientId, text);
    }

    @POST
    @Path("/all/{text}")
    @Produces({APPLICATION_JSON})
    public HttpResponse sendToAll(@NotNull @PathParam("text") String text) {
        return service.sendToAllClients(text);
    }
}


