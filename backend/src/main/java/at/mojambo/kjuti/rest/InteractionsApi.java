package at.mojambo.kjuti.rest;

import at.mojambo.kjuti.model.messaging.NotificationData;
import at.mojambo.kjuti.rest.model.ObjectResponse;
import at.mojambo.kjuti.rest.model.TData;
import at.mojambo.kjuti.service.DataService;
import at.mojambo.kjuti.service.InteractionService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Daniel Moser <office@mojambo.at>
 */
@Path("/")
public class InteractionsApi {

    @Inject
    InteractionService service;
    @Inject
    DataService dataService;

    @GET
    // count
    @Path("/ct/{fingerprint}")
    @Produces({APPLICATION_JSON})
    public ObjectResponse<Integer> getCount(@NotNull @PathParam("fingerprint") Long fingerprint) {
        return new ObjectResponse.Builder()
                .payload(service.getAllOpen(fingerprint).size())
                .build();
    }

    @GET
    // open interactions
    @Path("/o/{fingerprint}")
    @Produces({APPLICATION_JSON})
    public Response getOpen(@NotNull @PathParam("fingerprint") Long fingerprint) {
        return Response
                .ok()
                .entity(service.getAllOpen(fingerprint))
                .build();
    }
    @GET
    // interaction by qhash
    @Path("/{qhash}")
    @Produces({APPLICATION_JSON})
    public ObjectResponse<NotificationData> getByHash(@NotNull @PathParam("qhash") String qhash) {
        return new ObjectResponse.Builder()
                .payload(service.getNotificationDataByHash(qhash))
                .build();
    }

    @POST
    // answerhash + answer action index
    @Path("/{ahash}/{index}")
    @Produces({APPLICATION_JSON})
    public Response postHash(@NotNull @PathParam("ahash") String ahash, @NotNull @PathParam("index") String index) {
        service.handleAnswer(ahash, index);
        return Response.ok().build();
    }

    @POST
    // answerhash + answer action index
    @Path("/app/{ahash}/{index}")
    @Produces({APPLICATION_JSON})
    public Response postHashFromApp(@NotNull @PathParam("ahash") String ahash, @NotNull @PathParam("index") String index) {
        service.handleAnswerFromApp(ahash, index);
        return Response.ok().build();
    }

    @POST
    @Path("/reset/{fingerprint}")
    @Produces({APPLICATION_JSON})
    public Response reset(@NotNull @PathParam("fingerprint") Long fingerprint) {
        if (service.reset(fingerprint)) {
            return Response.ok().build();
        }
        return Response.noContent().build();
    }

    @PUT
    @Path("/tdata/{hash}")
    @Consumes({APPLICATION_JSON})
    public Response putTextData(@PathParam("hash") String hash, @NotNull TData textData) {
        dataService.handleTextData(hash, textData);
        return Response.ok().build();
    }
}

