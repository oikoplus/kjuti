package at.mojambo.kjuti.rest;

import at.mojambo.kjuti.model.dto.ClientDto;
import at.mojambo.kjuti.rest.model.ListResponse;
import at.mojambo.kjuti.rest.model.ObjectResponse;
import at.mojambo.kjuti.service.ClientService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.lang.reflect.InvocationTargetException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Daniel Moser <office@mojambo.at>
 */
@Path("/clients")
public class ClientRestApi {

    @Inject
    ClientService clientService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ListResponse<ClientDto> getAll() {
        return new ListResponse.Builder().payload(clientService.getAll()).build();
    }

    @GET
    @Path("/{fingerprint}")
    @Produces(MediaType.APPLICATION_JSON)
    public ObjectResponse<ClientDto> getClientByFingerprint(@NotNull @PathParam("fingerprint") Long fingerprint)  {
        if (clientService.checkFingerprintKnown(fingerprint)) {
                        return new ObjectResponse.Builder()
                    .payload(clientService.getClientDtoByFingerprint(fingerprint))
                    .build();
        } else {
            throw new NotFoundException();
        }
    }

    @POST
    @Produces({APPLICATION_JSON})
    @Consumes({APPLICATION_JSON})
    public ObjectResponse<ClientDto> postClient(@NotNull ClientDto clientDto) throws InvocationTargetException, IllegalAccessException {
        if (clientService.checkFingerprintKnown(clientDto.getFingerprint())) {
            return new ObjectResponse.Builder()
                    .payload(clientService.getClientByFingerprint(clientDto.getFingerprint()))
                    .build();
        } else {
            return new ObjectResponse.Builder()
                    .payload(clientService.createClient(clientDto))
                    .build();
        }
    }

}
