package at.mojambo.kjuti.rest.model;

import java.util.Map;

@SuppressWarnings("unused")
public class ObjectResponse<T> {
    private T payload;
    private Map<String, String> headers;

    public ObjectResponse() {
    }

    public T getPayload() {
        return this.payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public static class Builder {
        private final ObjectResponse response = new ObjectResponse();

        public Builder() {
        }

        public <T> ObjectResponse.Builder payload(T data) {
            this.response.setPayload(data);
            return this;
        }
        public <T> ObjectResponse.Builder headers(Map<String, String> headers) {
            this.response.setHeaders(headers);
            return this;
        }
        public <T> ObjectResponse<T> build() {
            return this.response;
        }
    }
}
