package at.mojambo.kjuti.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Daniel Moser <office@mojambo.at>
 */
@ApplicationPath("/")
public class RestApplication extends Application {
}
