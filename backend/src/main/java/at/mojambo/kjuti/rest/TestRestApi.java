package at.mojambo.kjuti.rest;

import at.mojambo.kjuti.model.enums.NotificationType;
import at.mojambo.kjuti.persistence.entity.QNodeEntity;
import at.mojambo.kjuti.persistence.repo.QNodeRepository;
import at.mojambo.kjuti.service.PushService;
import at.mojambo.kjuti.service.TestQNodeService;
import com.google.firebase.database.annotations.NotNull;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Daniel Moser <office@mojambo.at>
 */

@Path("/test")
public class TestRestApi {

  @Inject
  QNodeRepository qNodeRepository;
  @Inject
  TestQNodeService testService;

  @Inject
  PushService pushService;


  @POST
  @Path("/load/{type}")
  @Transactional
  @Produces({APPLICATION_JSON})
  public Response create(@PathParam("type") String type) {
    if (type.equals("")) {
      testService.loadTestTree(NotificationType.NONE);
    }
    NotificationType typeEnum;
    try {
      typeEnum = NotificationType.valueOf(type.toUpperCase());
    } catch (Exception e) {
      throw new BadRequestException(e);
    }
    testService.loadTestTree(typeEnum);
    List<QNodeEntity> all = qNodeRepository.loadAll();
    return Response.ok(all).build();
  }

  @POST
  @Path("/ping/{clientId}")
  @Transactional
  @Produces({APPLICATION_JSON})
  public Response ping(@NotNull @PathParam("clientId") Long clientId) {
    pushService.ping(clientId);
    return Response.ok().build();
  }

  @POST
  @Path("/cam/{clientId}")
  @Transactional
  @Produces({APPLICATION_JSON})
  public Response cam(@NotNull @PathParam("clientId") Long clientId) {
    pushService.cam(clientId);
    return Response.ok().build();
  }
}
