package at.mojambo.kjuti.rest.model;

@SuppressWarnings("unused")
public class QDataResponse<T> {
    private T payload;

    public T getPayload() {
        return this.payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
