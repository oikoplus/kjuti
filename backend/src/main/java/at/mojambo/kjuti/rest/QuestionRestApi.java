package at.mojambo.kjuti.rest;

import at.mojambo.kjuti.model.messaging.NotificationData;
import at.mojambo.kjuti.rest.model.ObjectResponse;
import at.mojambo.kjuti.service.QNodeService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Daniel Moser <office@mojambo.at>
 */
@Path("/question")
public class QuestionRestApi {

  @Inject
  QNodeService qNodeService;


  @GET
  @Path("/{qHash}")
  @Produces(MediaType.APPLICATION_JSON)
  public ObjectResponse<NotificationData> getClientByFingerprint(@NotNull @PathParam("qHash") String qHash) {
    return new ObjectResponse.Builder()
            .payload(qNodeService.getByQHash(qHash))
            .build();
  }
}
