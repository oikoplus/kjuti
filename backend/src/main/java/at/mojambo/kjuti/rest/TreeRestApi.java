package at.mojambo.kjuti.rest;

import at.mojambo.kjuti.persistence.entity.InteractionEntity;
import at.mojambo.kjuti.rest.model.ListResponse;
import at.mojambo.kjuti.rest.model.ObjectResponse;
import at.mojambo.kjuti.service.TreeService;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Daniel Moser <office@mojambo.at>
 */
@Path("/t")
public class TreeRestApi {

    @Inject
    TreeService treeService;

    @POST
    @Path("/setup/{id}")
    @Produces({APPLICATION_JSON})
    public ListResponse<InteractionEntity> setupTreeById(@NotNull @PathParam("id") String id) {
        List<InteractionEntity> result = treeService.setupInteractionsForTreeById(Long.parseLong(id), -1L);
        return new ListResponse.Builder()
                .payload(result)
                .build();
    }
    @POST
    @Path("/setup/{id}/u/{uid}")
    @Produces({APPLICATION_JSON})
    public ListResponse<InteractionEntity> setupTreeByIdForUserId(@NotNull @PathParam("id") Long id, @NotNull @PathParam("uid") Long uid) {
        List<InteractionEntity> result = treeService.setupInteractionsForTreeById(id, uid);
        return new ListResponse.Builder()
                .payload(result)
                .build();
    }
    @POST
    @Path("/start/{id}")
    @Produces({APPLICATION_JSON})
    public ListResponse<String> startTreeById(@NotNull @PathParam("id") String id) {
        List<String> result = treeService.startTreeById(Long.parseLong(id), -1L);
        return new ListResponse.Builder()
                .payload(result)
                .build();
    }
    @POST
    @Path("/start/{id}/u/{uid}")
    @Produces({APPLICATION_JSON})
    public ListResponse<String> startTreeForUserById(@NotNull @PathParam("id") Long id, @NotNull @PathParam("uid") Long uid) {
        List<String> result = treeService.startTreeById(id, uid);
        return new ListResponse.Builder()
                .payload(result)
                .build();
    }

    @PUT
    @Path("/new")
    @Produces({APPLICATION_JSON})
    @Consumes({APPLICATION_JSON})
    public ObjectResponse<Boolean> putTree() {
        return new ObjectResponse.Builder().payload(true).build();
    }

}
