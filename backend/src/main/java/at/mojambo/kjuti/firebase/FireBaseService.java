package at.mojambo.kjuti.firebase;

import at.mojambo.kjuti.model.messaging.Notification;
import at.mojambo.kjuti.model.messaging.NotificationActions;
import at.mojambo.kjuti.model.messaging.NotificationData;
import at.mojambo.kjuti.model.enums.NotificationType;
import at.mojambo.kjuti.persistence.entity.ClientEntity;
import at.mojambo.kjuti.persistence.entity.InteractionEntity;
import at.mojambo.kjuti.persistence.entity.QNodeEntity;
import at.mojambo.kjuti.service.ClientService;
import at.mojambo.kjuti.service.TestQNodeService;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import java.io.IOException;
import java.util.logging.Logger;

@ApplicationScoped
public class FireBaseService {

    @Inject
    TestQNodeService testQNodeService;
    @Inject
    ClientService clientService;

    private static final Logger LOGGER = Logger.getLogger(FireBaseService.class.getName());
    Gson gson = new Gson();

    public FireBaseService() {
        try {
            FirebaseOptions optionsDev = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.getApplicationDefault())
                    .build();

            FirebaseApp.initializeApp(optionsDev);
        } catch (IllegalStateException | IOException ex) {
            System.out.println("FirebaseApp is initialized.");
        }
    }

    private HttpResponse sendNotification(Notification notification) {
        HttpClient httpclient = HttpClientBuilder.create().build();
        Gson gson = new Gson();
        String body = gson.toJson(notification);
        HttpPost httppost = new HttpPost("https://fcm.googleapis.com/fcm/send");
        httppost.setHeader("Authorization", "key=AAAAxIhSbik:APA91bFUlYmDZr0JArKhypW6Tebc3DM9ETflIrRe40BLFu3ov2oDXMAQ8CtWSrlWQSbmudnT3FI6qL18setfBybebTcwMf1LJzMgS95KnueCAQIRD0ucv5nR6OgTfD62VfVJuM-C5IBo");
        httppost.setHeader("Content-Type", "application/json");
        httppost.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
        HttpResponse response;
        try {
            response = httpclient.execute(httppost);
            System.out.println(response);
        } catch (IOException ex) {
            throw new WebApplicationException("Could not POST to fcm: " + ex.getMessage());
        }
        return response;
    }

    public HttpResponse sendPush(ClientEntity entity, QNodeEntity qNodeEntity, String currentHash, boolean isFollowup) {
        LOGGER.info(">>>>>>> SENDING TO: " + entity.getNickname() + " ::: " + qNodeEntity.getText());

        Notification notification = new Notification(entity.getFirebaseToken());
        NotificationData data = generateNotificationData(qNodeEntity, currentHash);
        data.setTitle(qNodeEntity.getTitle());
        data.setFollowUp(isFollowup);
        data.setAnswerType(qNodeEntity.getAnswerType());
        notification.setData(data);
        return sendNotification(notification);
    }

     public NotificationData generateNotificationData(QNodeEntity qNodeEntity, String currentHash) {
         NotificationData data = new NotificationData(qNodeEntity.getTitle(), qNodeEntity.getAnswerType());
         data.setQuestion(qNodeEntity.getText());
         data.setQHash(qNodeEntity.getHash());
         data.setAnswerHash(currentHash);

         NotificationActions actions = new NotificationActions(
                 testQNodeService.getActionTextAndIndexFromChildrenIds(qNodeEntity.getChildren())
         );
         data.setJsonData(gson.toJson(actions));
        return data;
    }

    public HttpResponse sendSimplePush(ClientEntity entity, String text) {
        LOGGER.info(">>>>>>> SENDING SIMPLE TO: " + entity.getNickname() + ":::" + text);
        Notification notification = new Notification(entity.getFirebaseToken());
        NotificationData data = new NotificationData(text, NotificationType.NONE);
         data.setQuestion(text);
         data.setQHash("");
         data.setAnswerHash("");

         NotificationActions actions = new NotificationActions();
         data.setJsonData(gson.toJson(actions));
        data.setTitle("A Kju:Ti check in");
        data.setFollowUp(false);
        data.setAnswerType(NotificationType.NONE);
        notification.setData(data);
        return sendNotification(notification);
    }

    public NotificationData generateNotificationDataFromInteraction(InteractionEntity interactionEntity) {
        QNodeEntity qNodeEntity = testQNodeService.getQNodeById(interactionEntity.getQid());
        String currentHash = interactionEntity.getCurrentHash();
        return generateNotificationData(qNodeEntity, currentHash);
    }

    public HttpResponse sayThankyou(ClientEntity client) {
        LOGGER.info(">>>>>>> SAYING THANK YOU TO: " + client.getNickname());

        Notification notification = new Notification(client.getFirebaseToken());
        NotificationData data = new NotificationData("<3", NotificationType.CHOICE);
        data.setQuestion("Kju:Ti thanks you for your cooperation.");
        NotificationActions actions = new NotificationActions(
//                qNodeService.getActionTextAndIndexFromChildrenIds(new ArrayList<>())
        );

        data.setJsonData(gson.toJson(actions));
        notification.setData(data);
        return sendNotification(notification);
    }

    public void ping(Long clientId) {
        String token = clientService.getTokenById(clientId);
        Notification notification = new Notification(token);
        NotificationData data = new NotificationData("*ping*");
        NotificationActions actions = new NotificationActions();
        data.setJsonData(gson.toJson(actions));
        notification.setData(data);
        sendNotification(notification);
    }
}
